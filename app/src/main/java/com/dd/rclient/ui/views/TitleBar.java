package com.dd.rclient.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.dd.rclient.R;

/**
 * Created by: Administrator.
 * Created date: 2019/9/6.
 * Description: 自定义标题栏
 */
public class TitleBar extends LinearLayout {

    // 返回按钮
    private ImageView backImageBtn;
    // 标题文字
    private TextView titleText;
    // 下一步文字
    private TextView nextTextBtn;
    // 下一步图标
    private ImageView nextImageBtn;
    // 分割线
    private View divLine;

    public TitleBar(Context context) {
        super(context);
        init(context);
    }

    public TitleBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TitleBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    /**
     * 初始化
     *
     * @param context 上下文参数
     */
    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_title_bar, this);
        this.backImageBtn = view.findViewById(R.id.app_title_back_btn);
        this.titleText = view.findViewById(R.id.app_title_text);
        this.nextTextBtn = view.findViewById(R.id.app_title_next_text_btn);
        this.nextImageBtn = view.findViewById(R.id.app_title_next_icon_btn);
        this.divLine = view.findViewById(R.id.app_title_split_line);
        this.divLine.setVisibility(GONE);
    }

    /**
     * 显示或隐藏分割线,默认隐藏
     *
     * @param show true: 显示 false: 隐藏
     */
    public void showDivLine(boolean show) {
        this.divLine.setVisibility(show ? VISIBLE : GONE);
    }

    /**
     * 设置返回按钮资源
     *
     * @param res 图片资源
     */
    public void setBackImage(@DrawableRes int res) {
        this.backImageBtn.setImageResource(res);
    }

    /**
     * 设置标题文字
     *
     * @param text 字符串
     */
    public void setTitleText(String text) {
        this.titleText.setText(text);
    }

    /**
     * 设置下一步文字
     *
     * @param text 字符串
     */
    public void setNextText(String text) {
        this.nextTextBtn.setText(text);
    }

    /**
     * 设置下一步图片显示
     *
     * @param res 图片资源
     */
    public void setNextImage(@DrawableRes int res) {
        this.nextImageBtn.setImageResource(res);
    }

    /**
     * 设置返回按钮监听
     *
     * @param click 事件监听
     */
    public void setBackBtnOnClick(OnClickListener click) {
        this.backImageBtn.setOnClickListener(click);
    }

    /**
     * 设置下一步文字点击监听
     *
     * @param click 事件监听
     */
    public void setNextTextOnClick(OnClickListener click) {
        this.nextTextBtn.setOnClickListener(click);
    }

    /**
     * 设置下一步图片点击监听
     *
     * @param click 事件监听
     */
    public void setNextImageOnClick(OnClickListener click) {
        this.nextImageBtn.setOnClickListener(click);
    }
}
