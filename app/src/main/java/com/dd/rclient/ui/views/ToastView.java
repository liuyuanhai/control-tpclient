package com.dd.rclient.ui.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dd.rclient.MainApplication;
import com.dd.rclient.R;

/**
 * Created by: Administrator.
 * Created time: 2019/5/22.
 * Description: Toast提示框
 */
public enum ToastView {
    @SuppressLint("StaticFieldLeak") ins;
    private android.widget.Toast mToast;
    private TextView mTextView;

    /**
     * 显示Toast
     *
     * @param content  显示内容
     * @param gravity  显示位置
     * @param duration 显示时间
     */
    public void showToast(String content, int gravity, int duration) {
        Activity activity = MainApplication.ins().activity();
        if (activity != null) {
            if (mToast == null) {
                mToast = new android.widget.Toast(activity);
                mToast.setDuration(duration);
                mToast.setGravity(gravity, 0, 0);
                @SuppressLint("InflateParams")
                View view = LayoutInflater.from(activity).inflate(R.layout.view_toast, null);
                mTextView = view.findViewById(R.id.view_toast_tv);
                mToast.setView(view);
            }
            mTextView.setText(content);
            mToast.show();
        }
    }
}
