package com.dd.rclient.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.dd.rclient.R;
import com.dd.rclient.databinding.ActivityLoginBinding;
import com.dd.rclient.logger.Log;
import com.dd.rclient.net.Network;
import com.dd.rclient.net.entity.Login;
import com.dd.rclient.net.entity.LoginDataBean;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.ui.base.BaseActivity;
import com.dd.rclient.utils.MD5;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.utils.Text;
import com.dd.rclient.utils.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by: Administrator.
 * Created date: 2019/9/24.
 * Description: 登录界面
 */
public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements Callback<Login> {

    private ProgressDialog dialog;
    private String acc;
    private String pwd;
    private String device = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.ddPlatform/device.id";

    @Override
    protected int onLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void onObject() {

    }

    @Override
    protected void onView() {

    }

    @Override
    protected void onData() {

    }

    /**
     * 显示加载对话框
     */
    private void showLoading() {
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("正在登录......");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
    }

    /**
     * 登录
     */
    public void onLoginBtnClicked(View view) {
        acc = binding.inputAccount.getText().toString();
        pwd = binding.inputPassword.getText().toString();
        if (Text.empty(acc)) {
            Toast.show("请输入账号");
            return;
        }
        if (Text.empty(pwd)) {
            Toast.show("请输入密码");
            return;
        }
        String seri = Prefer.get(Settings.SIR_NUM, "");
        if (Text.empty(seri)) {
            seri = matchMD5();
        }
        Network.api().login(acc, pwd, seri).enqueue(this);
        showLoading();
    }

    /**
     * 生成MD5值
     *
     * @return 返回MD5值
     */
    private String matchMD5() {
        String deviceId = this.readDeviceId();
        if (Text.empty(deviceId)) {
            String data = this.deviceId() + Build.MANUFACTURER + Build.MODEL;
            deviceId = MD5.encrypt(data, 32, true);
            this.writeDeviceId(deviceId);
        }
        Prefer.put(Settings.SIR_NUM, deviceId);
        return deviceId;
    }

    @Override
    public void onResponse(@NonNull Call<Login> call, @NonNull Response<Login> response) {
        if (dialog != null) {
            dialog.cancel();
        }
        Login body = response.body();
        if (body != null) {
            if (body.getStatus() == 0) {
                LoginDataBean data = body.getData();
                String token = data.getToken();
                if (!Text.empty(token)) {
                    Prefer.put(Settings.NICK_NAME, data.getNickName());
                    Prefer.put(Settings.TAR_MID, data.getMid());
                    Prefer.put(Settings.USER_ACCOUNT, acc);
                    Prefer.put(Settings.USER_PASSWORD, pwd);
                    Prefer.put(Settings.TOKEN, token);
                    checkDeviceInfo();
                }
            } else {
                Toast.show(body.getMsg());
            }
        } else {
            Toast.show("服务器内部错误");
        }
    }

    @Override
    public void onFailure(@NonNull Call<Login> call, @NonNull Throwable t) {
        if (dialog != null) {
            dialog.cancel();
        }
        Toast.show("网络异常");
    }

    /**
     * 启动主界面
     */
    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 设备信息收集
     */
    private void checkDeviceInfo() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
            Call<Object> call = com.dd.rclient.net.Network.api().pushDeviceInfo(matchMD5(),
                    info.versionName, String.valueOf(info.versionCode), Build.VERSION.RELEASE,
                    Build.VERSION.SDK_INT, Build.MANUFACTURER, Build.MODEL, getSupportAbi());
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                    startMainActivity();
                }

                @Override
                public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                    Toast.show("网络异常");
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取支持的CPU架构信息
     *
     * @return 返回架构信息
     */
    private String getSupportAbi() {
        StringBuilder builder = new StringBuilder();
        String[] abis = Build.SUPPORTED_ABIS;
        for (String abi : abis) {
            if (builder.length() != 0) {
                builder.append(",");
            }
            builder.append(abi);
        }
        return builder.toString();
    }

    /**
     * 获取设备唯一标识
     *
     * @return 返回设备唯一标识的字符串
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String deviceId() {
        TelephonyManager manager = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
        String ime = null;
        if (manager != null) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ime = manager.getDeviceId(0);
                } else {
                    ime = manager.getDeviceId();
                }
                if (ime == null || ime.length() == 0) {
                    ime = manager.getSubscriberId();
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        return ime == null || ime.length() == 0 ? UUID.randomUUID().toString() : ime;
    }

    /**
     * 保持设备唯一识别码
     *
     * @param deviceId 识别码
     */
    private void writeDeviceId(String deviceId) {
        try {
            File file = new File(device);
            if (!file.exists()) {
                File dir = file.getParentFile();
                if (dir != null && !dir.exists()) {
                    if (dir.mkdirs()) {
                        Log.d("Directory " + dir.getAbsolutePath() + " create success");
                    }
                }
                if (file.createNewFile()) {
                    Log.d("File " + file.getAbsolutePath() + " create success");
                }
            }
            PrintWriter writer = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file)));
            writer.write(deviceId);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取设备唯一识别码
     *
     * @return 返回识别码或null
     */
    private String readDeviceId() {
        String deviceId = null;
        try {
            File file = new File(device);
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                deviceId = Text.clean(reader.readLine());
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deviceId;
    }
}
