package com.dd.rclient.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.dd.rclient.BuildConfig;
import com.dd.rclient.MainApplication;
import com.dd.rclient.R;
import com.dd.rclient.databinding.ActivitySplashBinding;
import com.dd.rclient.logger.Log;
import com.dd.rclient.net.entity.Login;
import com.dd.rclient.net.entity.LoginDataBean;
import com.dd.rclient.net.entity.Update;
import com.dd.rclient.net.entity.UpdateDataBean;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.threads.Worker;
import com.dd.rclient.ui.base.BaseActivity;
import com.dd.rclient.utils.MD5;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.utils.SuperPermission;
import com.dd.rclient.utils.Text;
import com.dd.rclient.utils.Version;
import com.dd.rclient.webrtc.service.SignalService;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by: Administrator.
 * Created date: 2018/10/16.
 * Description: 启动页
 */
@SuppressLint("CustomSplashScreen")
public class SplashActivity extends BaseActivity<ActivitySplashBinding> {

    private String[] step;
    private String[] permissions;
    private boolean canNext;
    private boolean canLogin;
    private Class<?> cls;
    private ProgressDialog dialog;
    private String url;
    private String account;
    private String password;
    private boolean canInit;
    private boolean checkRoot;
    private File device;
    private int checkState = 0;
    private DisplayMetrics metrics;

    @Override
    protected int onLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onObject() {
        device = new File(getFilesDir(), "device.id");
        cls = MainActivity.class;
        canInit = true;
        if (SignalService.isWorking()) {
            startNextActivity();
            canInit = false;
            return;
        }
        step = new String[]{
                "正在初始化...",
                "正在检查APP权限...",
                "正在检查网络链接...",
                "正在检查版本更新...",
                "正在检查账号信息...",
                "正在自动登录...",
                "正在检查设备信息...",
                "正在检查ROOT权限...",
                "初始化完成",
        };
        permissions = new String[]{
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WAKE_LOCK,
        };
    }

    @Override
    protected void onView() {
        Display display = getWindowManager().getDefaultDisplay();
        metrics = new DisplayMetrics();
        display.getRealMetrics(metrics);
    }

    @Override
    protected void onData() {
        if (BuildConfig.DEBUG) {
            new AlertDialog.Builder(this)
                    .setMessage("是否执行ROOT权限检查")
                    .setPositiveButton("检查", (dialog, which) -> {
                        checkRoot = true;
                        dialog.dismiss();
                        showUrlDialog();
                    })
                    .setNeutralButton("不检查", (dialog, which) -> {
                        checkRoot = false;
                        dialog.dismiss();
                        showUrlDialog();
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        } else {
            init();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkState == 1) {
            if (verifyAccessibilityService()) {
                checkState = 2;
                canNext = true;
            }
        }
    }

    /**
     * 现实URL切换对话框
     */
    @SuppressLint("InflateParams")
    private void showUrlDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.view_host_settings, null);
        EditText et = view.findViewById(R.id.et_host);
        String url = Prefer.get(Settings.IP, "154.31.254.230");
        et.setText(url);
        new AlertDialog.Builder(this)
                .setTitle("设置或选择服务器地址")
                .setView(view)
//                .setNegativeButton("测试地址", (dialog, which) -> {
//                    Prefer.put(Settings.URL, "http://192.168.0.105:10086/");
//                    com.lingin.rclient.net.Network.relplace("http://192.168.0.105:10010/");
//                    dialog.dismiss();
//                    init();
//                })
                .setPositiveButton("线上地址", (dialog, which) -> {
                    Prefer.put(Settings.URL, "http://45.253.65.23:10086/");
                    com.dd.rclient.net.Network.replace("http://45.253.65.23:10010/");
                    dialog.dismiss();
                    init();
                })
                .setNeutralButton("自定义", (dialog, which) -> {
                    String ip = et.getText().toString();
                    String socket = String.format("http://%s:10086/", ip);
                    String api = String.format("http://%s:10010/", ip);
                    Prefer.put(Settings.URL, socket);
                    Prefer.put(Settings.IP, ip);
                    com.dd.rclient.net.Network.replace(api);
                    dialog.dismiss();
                    init();
                })
                .setCancelable(false)
                .create()
                .show();
    }

    /**
     * 初始化
     */
    private void init() {
        if (canInit) {
            binding.splashAppVersion.setText(getString(R.string.app_version, Version.appVersionName()));
            Worker.execute(() -> {
                for (int i = 0; i < step.length; i++) {
                    int index = i;
                    runOnUiThread(() -> binding.initState.setText(step[index]));
                    doStep(index);
                    delay(200);
                }
                startNextActivity();
            });
        }
    }

    /**
     * 执行步骤
     *
     * @param index 步骤计数
     */
    private void doStep(int index) {
        if (index == 1) {
            requestPermission();
        } else if (index == 2) {
            isNetworkAvailable();
        } else if (index == 3) {
            checkNewVersion();
        } else if (index == 4) {
            checkAccountInfo();
        } else if (index == 5) {
            autoLogin();
        } else if (index == 6) {
            checkDeviceInfo();
        } else if (index == 7) {
            if (BuildConfig.DEBUG) {
                if (checkRoot) {
                    verifyRoot();
                }
            } else {
                verifyRoot();
            }
        } else {
            delay(1000);
        }
    }

    /**
     * 自动登录
     */
    private void autoLogin() {
        String seri = Prefer.get(Settings.SIR_NUM, "");
        if (Text.empty(seri)) {
            seri = matchMD5();
        }
//        if (canLogin) {
        canNext = false;
        com.dd.rclient.net.Network.api().login(account, password, seri).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(@NonNull Call<Login> call, @NonNull Response<Login> response) {
                Login body = response.body();
                if (body != null) {
                    if (body.getStatus() == 0) {
                        LoginDataBean data = body.getData();
                        String token = data.getToken();
                        if (!Text.empty(token)) {
                            Prefer.put(Settings.NICK_NAME, data.getNickName());
                            Prefer.put(Settings.TAR_MID, data.getMid());
                            Prefer.put(Settings.TOKEN, token);
                            cls = MainActivity.class;
                            canNext = true;
                            return;
                        }
                    }
                }
                canNext = true;
//                    cls = LoginActivity.class;
            }

            @Override
            public void onFailure(@NonNull Call<Login> call, @NonNull Throwable t) {
                canNext = true;
//                    cls = LoginActivity.class;
            }
        });
        waiting();
        //} //else {
//            cls = LoginActivity.class;
        //}
    }

    /**
     * 生成MD5值
     *
     * @return 返回MD5值
     */
    private String matchMD5() {
        String deviceId = this.readDeviceId();
        if (Text.empty(deviceId)) {
            String data = deviceId() + Build.MANUFACTURER + Build.MODEL;
            deviceId = MD5.encrypt(data, 32, true);
            this.writeDeviceId(deviceId);
        }
        Prefer.put(Settings.SIR_NUM, deviceId);
        return deviceId;
    }

    /**
     * 检查账号信息
     */
    private void checkAccountInfo() {
//        account = Prefer.get(Settings.USER_ACCOUNT, "");
//        password = Prefer.get(Settings.USER_PASSWORD, "");
//        canLogin = !Text.empty(account) || !Text.empty(password);
        canLogin = true;
    }

    /**
     * 设备信息收集
     */
    private void checkDeviceInfo() {
        canNext = false;
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
            // 保存信息
            Prefer.put(Settings.APP_VERSION_NAME, info.versionName);
            Prefer.put(Settings.APP_VERSION_CODE, String.valueOf(info.versionCode));
            Prefer.put(Settings.APP_SYSTEM_REL, Build.VERSION.RELEASE);
            Prefer.put(Settings.APP_SDK_INT, String.valueOf(Build.VERSION.SDK_INT));
            Prefer.put(Settings.APP_SYSTEM_MANUFACTURER, Build.MANUFACTURER);
            Prefer.put(Settings.APP_SYSTEM_MODEL, Build.MODEL);
            Prefer.put(Settings.APP_SYSTEM_ABI, getSupportAbi());
            // 分辨率数据
            if (metrics != null) {
                Prefer.put(Settings.SCREEN_WIDTH, metrics.widthPixels);
                Prefer.put(Settings.SCREEN_HEIGHT, metrics.heightPixels);
            }
            // 提交数据
            Call<Object> call = com.dd.rclient.net.Network.api().pushDeviceInfo(matchMD5(),
                    info.versionName, String.valueOf(info.versionCode), Build.VERSION.RELEASE,
                    Build.VERSION.SDK_INT, Build.MANUFACTURER, Build.MODEL, getSupportAbi());
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                    canNext = true;
                }

                @Override
                public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                    canNext = true;
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            canNext = true;
        }
        waiting();
    }

    /**
     * 获取支持的CPU架构信息
     *
     * @return 返回架构信息
     */
    private String getSupportAbi() {
        StringBuilder builder = new StringBuilder();
        String[] abis = Build.SUPPORTED_ABIS;
        for (String abi : abis) {
            if (builder.length() != 0) {
                builder.append(",");
            }
            builder.append(abi);
        }
        return builder.toString();
    }

    /**
     * 获取设备唯一标识
     *
     * @return 返回设备唯一标识的字符串
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String deviceId() {
        TelephonyManager manager = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
        String ime = null;
        if (manager != null) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ime = manager.getDeviceId(0);
                } else {
                    ime = manager.getDeviceId();
                }
                if (ime == null || ime.length() == 0) {
                    ime = manager.getSubscriberId();
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        return ime == null || ime.length() == 0 ? UUID.randomUUID().toString() : ime;
    }

    /**
     * 检查新版本
     */
    private void checkNewVersion() {
        canNext = false;
        Call<Update> call = com.dd.rclient.net.Network.api().checkUpdate(Version.cleanAppVersionName(), "ANDROID");
        call.enqueue(new Callback<Update>() {
            @Override
            public void onResponse(@NonNull Call<Update> call, @NonNull Response<Update> response) {
                Update body = response.body();
                if (body != null) {
                    UpdateDataBean data = body.getData();
                    if (data != null && data.isToUpdate()) {
                        url = data.getFileUri();
                        runOnUiThread(() -> showUpdateDialog("发现新版本V" + data.getVersion(), data.getVersionDesc()));
                    } else {
                        canNext = true;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Update> call, @NonNull Throwable t) {
                canNext = true;
            }
        });
        waiting();
    }

    /**
     * 网络链接检查
     */
    private void isNetworkAvailable() {
        canNext = false;
        Context context = getApplicationContext();
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            Network[] networks = connectivityManager.getAllNetworks();
            if (networks.length > 0) {
                for (Network network : networks) {
                    if (network != null) {
                        canNext = true;
                        break;
                    }
                }
            }
        }
        if (!canNext) {
            runOnUiThread(() -> showDialog("网络链接检查", "您的设备无法链接到互联网,请检查数据网络或WIFI后重试"));
        }
        waiting();
    }

    /**
     * Root权限检查
     */
    private void verifyRoot() {
        canNext = false;
        boolean hasRoot = SuperPermission.hasSuperPermission();
        Prefer.put(Settings.DEVICE_HAS_ROOT, hasRoot);
        if (!hasRoot) {
            if (!verifyAccessibilityService()) {
                checkState = 1;
                waiting();
                return;
            }
        }
        canNext = true;
        waiting();
    }

    /**
     * 检查无障碍是否打开
     */
    private boolean verifyAccessibilityService() {
        if (BuildConfig.DEBUG) {
            return true;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (!accessibilityManager.isEnabled()) {
            runOnUiThread(() -> new AlertDialog.Builder(this)
                    .setTitle("无障碍服务")
                    .setMessage("尚未开启无障碍服务, 是否立即前往开启?")
                    .setNegativeButton("去开启", (dialog, which) -> {
                        try {
                            startActivity(new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    })
                    .setCancelable(false)
                    .create()
                    .show());
            return false;
        }
        return true;
    }

    /**
     * 请求权限
     */
    private void requestPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            canNext = false;
            requestSelfPermission(permissions, (authorize, permissions) -> {
                if (authorize) {
                    canNext = true;
                } else {
                    showDialog("权限请求信息", "权限授权失败,请在设置中手动打开后重试");
                }
            });
            waiting();
        } else {
            Log.d("Requires permissions success");
        }
    }


    /**
     * 保持设备唯一识别码
     *
     * @param deviceId 识别码
     */
    private void writeDeviceId(String deviceId) {
        try {
            File file = device;
            if (!file.exists()) {
                File dir = file.getParentFile();
                if (dir != null && !dir.exists()) {
                    if (dir.mkdirs()) {
                        Log.d("Directory " + dir.getAbsolutePath() + " create success");
                    }
                }
                if (file.createNewFile()) {
                    Log.d("File " + file.getAbsolutePath() + " create success");
                }
            }
            PrintWriter writer = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file)));
            writer.write(deviceId);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取设备唯一识别码
     *
     * @return 返回识别码或null
     */
    private String readDeviceId() {
        String deviceId = null;
        try {
            File file = device;
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                deviceId = Text.clean(reader.readLine());
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deviceId;
    }

    /**
     * 等待
     */
    private void waiting() {
        while (!canNext) {
            delay(500);
        }
    }

    /**
     * 延时
     *
     * @param millis 时间毫秒
     */
    private void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示确定对话框
     *
     * @param title 标题
     * @param text  内容
     */
    private void showDialog(String title, String text) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(text)
                .setNegativeButton("确定", (dialogInterface, i) -> MainApplication.ins().quit())
                .create();
        dialog.show();
    }

    /**
     * 显示更新对话框
     *
     * @param title   标题
     * @param content 内容
     */
    private void showUpdateDialog(String title, String content) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(content)
                .setNegativeButton("立即更新", (dialogInterface, i) -> showProcessDialog())
                .create();
        dialog.show();
    }

    /**
     * 显示进度对话框
     */
    private void showProcessDialog() {
        dialog = new ProgressDialog(this);
        dialog.setProgress(0);
        dialog.setMax(100);
        dialog.setCancelable(false);
        dialog.setTitle("正在下载");
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.show();
        downloadApk(url);
    }

    /**
     * 下载APK
     */
    @SuppressLint("Range")
    private void downloadApk(String url) {
        final File apk = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.format("%s.apk", System.currentTimeMillis()));
        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationUri(Uri.fromFile(apk));
        final long enqueue = dm.enqueue(request);

        // 注册下载完成广播
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (enqueue == reference) {
                    // 下载完成后取消监听
                    context.unregisterReceiver(this);
                    // 安装应用
                    startInstallNewVersion(apk);
                }
            }
        }, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        // 获取下载进度
        Worker.execute(() -> {
            boolean success = false;
            while (!success) {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(enqueue);
                Cursor cursor = dm.query(query);
                cursor.moveToFirst();
                // 已下载
                int downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                // 总大小
                int total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                // 获取下载状态
                int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                    // 下载完成
                    dialog.dismiss();
                    success = true;
                } else if (status == DownloadManager.STATUS_FAILED) {
                    // 下载出错
                    dialog.dismiss();
                    showDownloadFailDialog();
                }
                cursor.close();
                // 处理下载进度
                final int value = (int) ((downloaded * 100L) / total);
                runOnUiThread(() -> dialog.setProgress(value));
                // 暂停100ms
                delay(100);
            }
        });
    }

    /**
     * 显示下载失败对话框
     */
    private void showDownloadFailDialog() {
        runOnUiThread(() -> {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("下载出错")
                    .setMessage("尝试重新下载？")
                    .setNegativeButton("重新下载", (dialogInterface, i) -> showProcessDialog())
                    .create();
            dialog.show();
        });
    }

    /**
     * 跳转安装
     *
     * @param apk 下载文件
     */
    private void startInstallNewVersion(File apk) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".FileProvider", apk);
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 启动主界面
     */
    private void startNextActivity() {
        startActivity(new Intent(this, cls)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}
