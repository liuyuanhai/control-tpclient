package com.dd.rclient.ui.activity;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.dd.rclient.BuildConfig;
import com.dd.rclient.R;
import com.dd.rclient.daemon.DaemonEnv;
import com.dd.rclient.daemon.IntentWrapper;
import com.dd.rclient.databinding.ActivityMainBinding;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.threads.Worker;
import com.dd.rclient.ui.base.BaseActivity;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.webrtc.OnSocketConnectStateChangeListener;
import com.dd.rclient.webrtc.service.SignalService;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by: Administrator.
 * Created date: 2019/8/25.
 * Description: 主界面
 */
public class MainActivity extends BaseActivity<ActivityMainBinding> implements OnSocketConnectStateChangeListener {

    private static final int CAPTURE_PERMISSION_REQUEST_CODE = 910;
    private DisplayMetrics metrics;
    private long time = -1L;
    private Disposable subscribe;
    private boolean initiative;
    //    private AlertDialog dlg;
    private boolean isConnect = false;
    //    private AlertDialog connectDlg;
//    private AlertDialog stateDialog;
//    private AlertDialog sysDialog;
    private PowerManager.WakeLock wakeLock;
    private boolean isScreenOn = false;
    private long touch;
    private String url;
//    private ProgressDialog dialog;
//    private long stamp;

    @Override
    protected int onLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onObject() {
        SignalService.setOnSocketConnectStateChangeListener(this);
        initScreenSize();
    }

    @Override
    protected void onView() {
//        binding.nowLoginAccount.setText(Prefer.get(Settings.NICK_NAME, Prefer.get(Settings.USER_ACCOUNT, "")));
//        if (Prefer.get(Settings.SET_SYSTEM, false)) {
//            String text = "检测到您上次挂机时挂机服务多次被系统关闭，为保证APP正常挂机，请逐步检查以下操作：" +
//                    "\n1.将此APP添加到锁屏清理白名单。" +
//                    "\n2.允许APP的后台运行。" +
//                    "\n3.允许APP的后台启动与进程唤醒。" +
//                    "\n4.将APP添加到电池优化忽略名单中。";
//            showSetSysDialog(text);
//        }
    }

    @Override
    protected void onData() {
        if (SignalService.isWorking() && SignalService.ins().isOnline()) {
            getSaveTime();
            if (!binding.autoWaitBtn.isWorking()) {
                binding.autoWaitBtn.startRecord();
            }
            if (subscribe == null) {
                updateTimeText();
            }
        }
        startDataService();
        // 3秒后开启自动挂机
        binding.getRoot().postDelayed(() -> onStartAutoWaitClicked(binding.autoWaitBtn), 3000);

        // 记录启动次数
        Prefer.put(Settings.BOOT_COUNT, Prefer.get(Settings.BOOT_COUNT, 0) + 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SignalService.isWorking()) {
            startDataService();
        }
        if (SignalService.isWorking() && SignalService.ins().isOnline()) {
            getSaveTime();
            if (SignalService.ins() != null && SignalService.ins().isConnected()) {
                binding.mecConnectState.setText("控制端已连接");
            } else {
                binding.mecConnectState.setText(isConnect ? "已连接到服务器,等待控制端连接" : "正在连接服务器...");
            }
        } else {
            if (Prefer.get(Settings.ONLINE_STATE, false)) {
                binding.mecConnectState.setText(isConnect ? "已连接到服务器,等待控制端连接" : "正在连接服务器...");
            } else {
                binding.mecConnectState.setText("");
            }
        }
        brightScreen();
        int count = Prefer.get(Settings.BOOT_COUNT, 0);
        if (count <= 1) {
            // 跳转桌面
            binding.getRoot().postDelayed(() ->
                    startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME)), 18000);
        }
//        stamp = System.currentTimeMillis();
//        binding.getRoot().postDelayed(() -> {
//            if (System.currentTimeMillis() - stamp > 15000) {
//                int count = Prefer.get(Settings.BOOT_COUNT, 0);
//                if (count <= 1) {
//                startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
//                }
//            }
//        }, 15500);
    }

    /**
     * 获取本地保存的时间
     */
    private void getSaveTime() {
        long old = Prefer.get(Settings.TIME, 0L);
        long now = System.currentTimeMillis() - old;
        this.time = now / 1000;
    }

    /**
     * 获取屏幕参数
     */
    private void initScreenSize() {
        int width = Prefer.get(Settings.SCREEN_WIDTH, 0);
        int height = Prefer.get(Settings.SCREEN_HEIGHT, 0);
        if (width == 0 && height == 0) {
            Display display = getWindowManager().getDefaultDisplay();
            metrics = new DisplayMetrics();
            display.getRealMetrics(metrics);
        }
    }

    /**
     * 启动数据服务
     */
    private void startDataService() {
        if (SignalService.getData() == null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                MediaProjectionManager mediaProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
                if (mediaProjectionManager != null) {
                    startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQUEST_CODE);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != CAPTURE_PERMISSION_REQUEST_CODE) {
            return;
        }
        if (Prefer.get(Settings.SCREEN_CAP_STATE, true) && resultCode == RESULT_CANCELED) {
//            new AlertDialog.Builder(this)
//                    .setMessage("请允许此程序获取您的屏幕内容后继续")
//                    .setNegativeButton("确定", (dialog, which) -> dialog.dismiss())
//                    .setCancelable(false)
//                    .create()
//                    .show();
            return;
        }
        Prefer.put(Settings.SCREEN_CAP_STATE, false);
        Prefer.put(Settings.RUNNING_STATE, true);
        SignalService.setData(data);
        if (metrics != null) {
            Prefer.put(Settings.SCREEN_WIDTH, metrics.widthPixels);
            Prefer.put(Settings.SCREEN_HEIGHT, metrics.heightPixels);
        }
        if (!SignalService.isWorking()) {
            SignalService.sShouldStopService = false;
            DaemonEnv.startServiceMayBind(SignalService.class);
        }
        if (Prefer.get(Settings.FIRST_START, true)) {
            Prefer.put(Settings.FIRST_START, false);
            IntentWrapper.whiteListMatters(this, "远程联机服务的持续运行");
        }
    }

    /**
     * 亮屏
     */
    private void brightScreen() {
        touch = System.currentTimeMillis();
        if (!isScreenOn) {
            isScreenOn = true;
            breathScreen(-1);
            Worker.execute(() -> {
                while (System.currentTimeMillis() - touch < (30 * 1000)) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                breathScreen(1);
            });
        }
    }

    /**
     * 设置屏幕亮度
     */
    private void breathScreen(int light) {
        runOnUiThread(() -> {
            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
            layoutParams.screenBrightness = light <= 0 ? -1.0f : light / 255f;
            getWindow().setAttributes(layoutParams);
            isScreenOn = false;
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (subscribe != null && !subscribe.isDisposed()) {
            subscribe.dispose();
        }
    }

    @Override
    public void onBackPressed() {
        IntentWrapper.onBackPressed(this);
    }

    /**
     * 启动定时器刷新时间
     */
    private void updateTimeText() {
        if (subscribe == null) {
            subscribe = Observable.interval(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        time += 1;
                        if (binding.autoWaitBtn.isWorking()) {
                            binding.autoWaitBtn.updateText(toTimeString(time));
                        }
                    });
        }
    }

    /**
     * 转换为之间字符串
     *
     * @param time 时间秒
     * @return 返回时分秒的时间字符串
     */
    private String toTimeString(long time) {
        long sec = time % 60;
        long minute = time / 60;
        long min = minute % 60;
        long hour = minute / 60;
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, min, sec);
    }

    /**
     * 开始/结束 挂机
     */
    @SuppressLint("WakelockTimeout")
    public void onStartAutoWaitClicked(View view) {
        brightScreen();
        if (SignalService.isWorking() && isConnect) {
//            new AlertDialog.Builder(this)
//                    .setMessage("是否结束挂机?")
//                    .setNegativeButton("确定", (dialog, which) -> {
//                        this.initiative = true;
//                        stop();
//                        Toast.show(String.format("本次挂机时长: %s", toTimeString(time)));
//                        dialog.dismiss();
//                    })
//                    .setPositiveButton("取消", (dialog, which) -> dialog.dismiss())
//                    .setCancelable(false)
//                    .create()
//                    .show();
        } else if (SignalService.isWorking() && !isConnect && !SignalService.ins().isConnecting()) {
            // 记录重连次数
            Prefer.put(Settings.RESTART_COUNT, 0);
            // 记录链接时间
            Prefer.put(Settings.START_TIME, System.currentTimeMillis());
            SignalService.ins().connect();
        } else if (!SignalService.isWorking()) {
            startDataService();
        } else {
//            if (connectDlg != null) {
//                connectDlg.dismiss();
//            }
//            connectDlg = new AlertDialog.Builder(this)
//                    .setMessage("正在连接服务器,请勿重复操作")
//                    .setNegativeButton("确定", (dialog, which) -> dialog.dismiss())
//                    .setCancelable(false)
//                    .create();
//            connectDlg.show();
        }
    }

    /**
     * 退出程序
     */
    public void onExitAppClicked(View view) {
        brightScreen();
//        new AlertDialog.Builder(this)
//                .setMessage("是否结束挂机并退出程序?")
//                .setNegativeButton("确定", (dialog, which) -> {
//                    this.initiative = true;
//                    stop();
//                    if (SignalService.isWorking()) {
//                        SignalService.stopService();
//                    }
//                    dialog.dismiss();
//                    MainApplication.ins().quit();
//                })
//                .setPositiveButton("取消", (dialog, which) -> dialog.dismiss())
//                .setCancelable(false)
//                .create()
//                .show();
    }

    /**
     * 后台运行
     */
    public void onRunBackgroundClicked(View view) {
        brightScreen();
        if (SignalService.isWorking()) {
            startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
        } else {
//            new AlertDialog.Builder(this)
//                    .setMessage("处于挂机状态中即可切换至后台运行")
//                    .setNegativeButton("确定", (dialog, which) -> dialog.dismiss())
//                    .setCancelable(false)
//                    .create()
//                    .show();
        }
    }

    /**
     * 解绑设备
     */
    public void onUnlockAccount(View view) {
//        brightScreen();
//        new AlertDialog.Builder(this)
//                .setMessage("需要解除与此设备的绑定?")
//                .setNegativeButton("确定", (dialog, which) -> {
//                    dialog.dismiss();
//                    Network.api().unlock(Prefer.get(Settings.SIR_NUM, "")).enqueue(new Callback<Unbind>() {
//                        @Override
//                        public void onResponse(@NonNull Call<Unbind> call, @NonNull Response<Unbind> response) {
//                            Unbind body = response.body();
//                            if (body != null) {
//                                String data = body.getData();
//                                runOnUiThread(() -> new AlertDialog.Builder(MainActivity.this)
//                                        .setMessage(data)
//                                        .setNegativeButton("确定", (dialog, which) -> {
//                                            MainActivity.this.initiative = true;
//                                            stop();
//                                            if (SignalService.isWorking()) {
//                                                SignalService.stopService();
//                                            }
//                                            Prefer.remove(Settings.USER_ACCOUNT);
//                                            Prefer.remove(Settings.USER_PASSWORD);
//                                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                                            finish();
//                                            dialog.dismiss();
//                                        })
//                                        .setPositiveButton("取消", (dialog, which) -> dialog.dismiss())
//                                        .setCancelable(false)
//                                        .create()
//                                        .show());
//                            }
//                            Toast.show("解除绑定失败");
//                        }
//
//                        @Override
//                        public void onFailure(@NonNull Call<Unbind> call, @NonNull Throwable t) {
//                            Toast.show("解除绑定失败");
//                        }
//                    });
//                })
//                .setPositiveButton("取消", (dialog, which) -> dialog.dismiss())
//                .setCancelable(false)
//                .create()
//                .show();
    }

    /**
     * 切换账号
     */
    public void onChangeAccountClicked(View view) {
        brightScreen();
//        new AlertDialog.Builder(this)
//                .setMessage("切换账号将会结束您当前的挂机,是否继续?")
//                .setNegativeButton("确定", (dialog, which) -> {
//                    this.initiative = true;
//                    stop();
//                    if (SignalService.isWorking()) {
//                        SignalService.stopService();
//                    }
//                    Prefer.remove(Settings.USER_ACCOUNT);
//                    Prefer.remove(Settings.USER_PASSWORD);
//                    startActivity(new Intent(this, LoginActivity.class));
//                    finish();
//                    dialog.dismiss();
//                })
//                .setPositiveButton("取消", (dialog, which) -> dialog.dismiss())
//                .setCancelable(false)
//                .create()
//                .show();
    }

    /**
     * 停止
     */
    private void stop() {
        brightScreen();
        if (SignalService.isWorking()) {
            SignalService.ins().close();
        }
        if (subscribe != null && !subscribe.isDisposed()) {
            subscribe.dispose();
            subscribe = null;
        }
        if (binding.autoWaitBtn.isWorking()) {
            binding.autoWaitBtn.stopRecord();
        }
        Prefer.remove(Settings.TIME);
        // 删除重连记录
        Prefer.remove(Settings.RESTART_COUNT);
        // 删除链接时间
        Prefer.remove(Settings.START_TIME);
        binding.mecConnectState.setText("");
        isConnect = false;
    }

    /**
     * 显示重连对话框
     *
     * @param fast 提示信息前段
     */
    private void showReconnectDialog(String fast) {
        brightScreen();
        this.isConnect = false;
//        runOnUiThread(() -> {
//            if (this.connectDlg != null) {
//                this.connectDlg.dismiss();
//            }
//            if (dlg != null) {
//                dlg.dismiss();
//            }
//            dlg = new AlertDialog.Builder(this)
//                    .setMessage(String.format("%s,正在重新连接......", fast))
//                    .setNegativeButton("立即连接", (dialog, which) -> {
//                        dialog.dismiss();
//                        SignalService.ins().reconnect();
//                    })
//                    .setPositiveButton("取消连接", (dialog, which) -> {
//                        runOnUiThread(this::stop);
//                        dialog.dismiss();
//                    })
//                    .setCancelable(false)
//                    .create();
//            dlg.show();
//        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStartConnect() {
        isConnect = false;
        runOnUiThread(() -> binding.mecConnectState.setText("正在连接服务器..."));
    }

    @Override
    public void onConnected() {
        this.isConnect = true;
        this.time = -1L;
        this.initiative = false;
//        if (this.connectDlg != null) {
//            this.connectDlg.dismiss();
//        }
//        if (this.dlg != null) {
//            this.dlg.dismiss();
//        }
//        if (this.sysDialog != null) {
//            this.sysDialog.dismiss();
//        }
        Prefer.put(Settings.TIME, System.currentTimeMillis());
        if (!binding.autoWaitBtn.isWorking()) {
            binding.autoWaitBtn.startRecord();
        }
        runOnUiThread(() -> binding.mecConnectState.setText("已连接到服务器,等待控制端连接"));
        brightScreen();
        updateTimeText();

        int count = Prefer.get(Settings.BOOT_COUNT, 0);
        if (count > 1) {
            // 跳转桌面
            runOnUiThread(() -> startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME)));
//            binding.getRoot().postDelayed(() ->
//                    startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME)), 10000);
        }
    }

    @Override
    public void onConnectError() {
        runOnUiThread(() -> binding.mecConnectState.setText("服务器连接失败"));
        showReconnectDialog("连接服务器失败");
    }

    @Override
    public void onConnectTimeout() {
        runOnUiThread(() -> binding.mecConnectState.setText("服务器连接超时"));
        showReconnectDialog("连接服务器超时");
    }

    @Override
    public void onDisconnect() {
        if (initiative) {
            return;
        }
        runOnUiThread(() -> binding.mecConnectState.setText("服务器连接已断开"));
        showReconnectDialog("您已和服务器断开链接");
    }

    @Override
    public void onMECConnectState(boolean connected) {
        runOnUiThread(() -> {
            brightScreen();
            if (connected) {
                binding.mecConnectState.setText("控制端已连接");
                // 跳转桌面
                startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
            } else {
                binding.mecConnectState.setText("已连接到服务器,等待控制端连接");
            }
        });
    }

    @Override
    public void onBusy() {
        initiative = true;
        runOnUiThread(() -> {
            binding.mecConnectState.setText("连接异常,正在重新连接...");
            SignalService.ins().reconnect();
        });
        showReconnectDialog("连接异常");
    }

    @Override
    public void onAuthStateError(String text) {
//        runOnUiThread(() -> {
//            stop();
//            if (stateDialog != null) {
//                stateDialog.dismiss();
//            }
//            stateDialog = new AlertDialog.Builder(this)
//                    .setMessage(text)
//                    .setNegativeButton("确定", (dialog, which) -> {
//                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                        finish();
//                    })
//                    .setCancelable(false)
//                    .create();
//            stateDialog.show();
//        });
    }

    @Override
    public void onSystemError(String text) {
        runOnUiThread(() -> {
            initiative = true;
//            stop();
            binding.mecConnectState.setText("正在连接服务器...");
            isConnect = false;
            // 异常不显示
            if (text.contains("Exception")) {
                return;
            }
//            if (sysDialog != null) {
//                sysDialog.dismiss();
//            }
//            sysDialog = new AlertDialog.Builder(this)
//                    .setMessage(text)
//                    .setNegativeButton("确定", (dialog, which) -> dialog.dismiss())
//                    .setCancelable(false)
//                    .create();
//            sysDialog.show();
        });
    }

    @SuppressLint("WakelockTimeout")
    @Override
    public void needSetSystem() {
        if (this.wakeLock == null) {
            initWakeLock();
        }
        if (this.wakeLock != null) {
            this.wakeLock.acquire();
        }
        Prefer.put(Settings.SET_SYSTEM, true);
//        String text = "检测到挂机服务多次被系统关闭，为保证APP正常挂机，请逐步检查以下操作：" +
//                "\n1.将此APP添加到锁屏清理白名单。" +
//                "\n2.允许APP的后台运行。" +
//                "\n3.允许APP的后台启动与进程唤醒。" +
//                "\n4.将APP添加到电池优化忽略名单中。";
//        showSetSysDialog(text);
    }

    /**
     * 初始化屏幕唤醒策略
     */
    private void initWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null) {
            wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getString(R.string.app_name));
        }
    }

    /**
     * 显示系统设置对话框
     *
     * @param text 文本
     */
    private void showSetSysDialog(String text) {
//        Dialog sys = new AlertDialog.Builder(this)
//                .setMessage(text)
//                .setNegativeButton("确定", (dialog, which) -> {
//                    Prefer.put(Settings.SET_SYSTEM, false);
//                    if (this.wakeLock != null) {
//                        this.wakeLock.release();
//                    }
//                    dialog.dismiss();
//                })
//                .setCancelable(false)
//                .create();
//        sys.show();
    }

    @Override
    public void needUpdate(String message, String url, String version) {
        // 不显示断开链接对话框
        initiative = true;
        this.url = url;
        runOnUiThread(() -> {
            // 停止重连
            this.stop();
            showUpdateDialog("发现新版本V" + version, message);
        });
    }

    /**
     * 显示更新对话框
     *
     * @param title   标题
     * @param content 内容
     */
    private void showUpdateDialog(String title, String content) {
//        AlertDialog dialog = new AlertDialog.Builder(this)
//                .setCancelable(false)
//                .setTitle(title)
//                .setMessage(content)
//                .setNegativeButton("立即更新", (dialogInterface, i) -> showProcessDialog())
//                .create();
//        dialog.show();
    }

    /**
     * 显示进度对话框
     */
    private void showProcessDialog() {
//        dialog = new ProgressDialog(this);
//        dialog.setProgress(0);
//        dialog.setMax(100);
//        dialog.setCancelable(false);
//        dialog.setTitle("正在下载");
//        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        dialog.show();
//
//        downloadApk(url);
//        DownloadManager.ins().download(url, this);
    }

    /**
     * 下载APK
     */
    @SuppressLint("Range")
    private void downloadApk(String url) {
        final File apk = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.format("%s.apk", System.currentTimeMillis()));
        DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationUri(Uri.fromFile(apk));
        final long enqueue = dm.enqueue(request);

        // 注册下载完成广播
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (enqueue == reference) {
                    // 下载完成后取消监听
                    context.unregisterReceiver(this);
                    // 安装应用
                    startInstallNewVersion(apk);
                }
            }
        }, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        // 获取下载进度
        Worker.execute(() -> {
            boolean success = false;
            while (!success) {
                android.app.DownloadManager.Query query = new android.app.DownloadManager.Query();
                query.setFilterById(enqueue);
                Cursor cursor = dm.query(query);
                cursor.moveToFirst();
                // 已下载
                int downloaded = cursor.getInt(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                // 总大小
                int total = cursor.getInt(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                // 获取下载状态
                int status = cursor.getInt(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_STATUS));
                if (status == android.app.DownloadManager.STATUS_SUCCESSFUL) {
                    // 下载完成
//                    dialog.dismiss();
                    success = true;
                } else if (status == android.app.DownloadManager.STATUS_FAILED) {
                    // 下载出错
//                    dialog.dismiss();
                    showDownloadFailDialog();
                }
                cursor.close();
                // 处理下载进度
                final int value = (int) ((downloaded * 100L) / total);
//                runOnUiThread(() -> dialog.setProgress(value));
                // 暂停100ms
                delay(100);
            }
        });
    }

    /**
     * 延时
     *
     * @param millis 时间毫秒
     */
    private void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示下载失败对话框
     */
    private void showDownloadFailDialog() {
//        AlertDialog dialog = new AlertDialog.Builder(this)
//                .setCancelable(false)
//                .setTitle("下载出错")
//                .setMessage("尝试重新下载？")
//                .setNegativeButton("重新下载", (dialogInterface, i) -> showProcessDialog())
//                .create();
//        dialog.show();
    }

    /**
     * 跳转安装
     *
     * @param apk 下载文件
     */
    private void startInstallNewVersion(File apk) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".FileProvider", apk);
            intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        brightScreen();
        return super.onTouchEvent(event);
    }
}
