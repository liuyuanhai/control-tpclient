package com.dd.rclient.ui.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.annotation.Nullable;

import com.dd.rclient.utils.Pixel;

/**
 * Created by: Administrator.
 * Created date: 2019/9/26.
 * Description: 圆形按钮
 */
public class CircleButton extends View {

    private Paint paint;
    private Paint circlePaint;
    private Paint textPaint;
    private Paint circleStrokePaint;
    private RectF rectF;
    private float strokeWidth;
    private float arcValue;
    private int textSize;
    private SweepGradient gradient;
    private Matrix matrix;
    private String timeText;
    private int width;
    private int height;
    private boolean canDraw = false;

    public CircleButton(Context context) {
        super(context);
        init();
    }

    public CircleButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * 初始化
     */
    private void init() {

        strokeWidth = Pixel.dp2px(16);
        textSize = Pixel.sp2px(28);
        timeText = "开始挂机";

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.WHITE);
        circlePaint.setStyle(Paint.Style.FILL);

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(0xff2196f3);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setStyle(Paint.Style.FILL);

        circleStrokePaint = new Paint();
        circleStrokePaint.setAntiAlias(true);
        circleStrokePaint.setColor(0xff2196f3);
        circleStrokePaint.setStyle(Paint.Style.STROKE);
        circleStrokePaint.setStrokeWidth(strokeWidth);

        initAnim();
    }

    /**
     * 开始挂机
     */
    public void startRecord() {
        if (!canDraw) {
            timeText = "00:00:00";
            canDraw = true;
        }
    }

    /**
     * 结束挂机
     */
    public void stopRecord() {
        if (canDraw) {
            timeText = "开始挂机";
            canDraw = false;
        }
    }

    /**
     * 是否处于运行中
     *
     * @return true: 运行中 false: 停止状态
     */
    public boolean isWorking() {
        return this.canDraw;
    }

    /**
     * 更新时间
     *
     * @param text 时间字符串
     */
    public void updateText(String text) {
        this.timeText = text;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (width == 0 || height == 0) {
            width = getWidth();
            height = getHeight();
        }
        if (matrix == null) {
            matrix = new Matrix();
        }
        if (rectF == null) {
            rectF = new RectF(strokeWidth, strokeWidth, width - strokeWidth, height - strokeWidth);
        }
        if (gradient == null) {
            gradient = new SweepGradient(width >> 1, height >> 1, new int[]{0xffffffff, 0xff2196f3}, null);
            paint.setShader(gradient);
        }
        matrix.setRotate(350f + arcValue, width >> 1, height >> 1);
        gradient.setLocalMatrix(matrix);
        canvas.drawCircle(width >> 1, height >> 1, width >> 1, circlePaint);
        textPaint.setTextSize(Pixel.sp2px(28));
        canvas.drawText(timeText, width >> 1, (height >> 1) + textSize / 3, textPaint);
        if (canDraw) {
            canvas.drawArc(rectF, arcValue, 270f, false, paint);
            textPaint.setTextSize(Pixel.sp2px(18));
            canvas.drawText("点击结束", width >> 1, (height / 2 + textSize) + textSize / 3, textPaint);
            textPaint.setTextSize(Pixel.sp2px(16));
            canvas.drawText("本次挂机时长", width >> 1, ((height >> 1) - textSize), textPaint);
        } else {
            canvas.drawArc(rectF, 0, 360f, false, circleStrokePaint);
        }
    }

    /**
     * 初始化动画
     */
    private void initAnim() {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 360f).setDuration(5000);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(-1);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(animation -> {
            arcValue = (float) animation.getAnimatedValue();
            invalidate();
        });
        animator.start();
    }
}
