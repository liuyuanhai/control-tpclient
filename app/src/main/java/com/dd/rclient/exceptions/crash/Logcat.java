package com.dd.rclient.exceptions.crash;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;

import com.dd.rclient.MainApplication;
import com.dd.rclient.logger.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Logcat {

    private static PrintWriter writer;

    public static void write(String arg) {
        if (writer == null) {
            String time = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.getDefault()).format(new Date(System.currentTimeMillis()));
            File file = new File(Environment.getExternalStorageDirectory(), String.format("/crash/debug-%s.log", time));
            try {
                writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
                writer.println(time);
                dumpDeviceInfo(writer);
            } catch (Exception e) {
                Log.e("dump exception failed: %s", e);
            }
        }
        writer.println(arg);
        writer.flush();
    }

    /**
     * 收集设备信息
     *
     * @param writer 输出流
     * @throws PackageManager.NameNotFoundException 调用 {@link PackageManager#getPackageInfo(String, int)} 时可能会出现的异常
     */
    private static void dumpDeviceInfo(PrintWriter writer) throws PackageManager.NameNotFoundException {
        Activity context = MainApplication.ins().activity();
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
        writer.println("App Version: " + packageInfo.versionName + "_" + packageInfo.versionCode);
        writer.println("Android OS Version: " + Build.VERSION.RELEASE + "_" + Build.VERSION.SDK_INT);
        writer.println("Device Vendor: " + Build.MANUFACTURER);
        writer.println("Device Mode: " + Build.MODEL);
        writer.println("Device CPU API: " + Build.CPU_ABI);
    }
}
