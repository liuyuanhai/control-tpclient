package com.dd.rclient.net.entity;

import java.util.List;

/**
 * Created by: Administrator.
 * Created at: 2022/4/13.
 * Description:
 */
public class Step {

    private int index;
    private List<Point> steps;
    private long duration;

    public Step() {
    }

    public Step(int index, List<Point> steps, long duration) {
        this.index = index;
        this.steps = steps;
        this.duration = duration;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Point> getSteps() {
        return steps;
    }

    public void setSteps(List<Point> steps) {
        this.steps = steps;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
