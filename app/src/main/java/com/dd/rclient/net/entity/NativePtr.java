package com.dd.rclient.net.entity;

import java.util.List;

/**
 * Created by: Administrator.
 * Created at: 2022/4/13.
 * Description:
 */
public class NativePtr {

    private List<Step> ptr;

    public NativePtr() {
    }

    public NativePtr(List<Step> ptr) {
        this.ptr = ptr;
    }

    public List<Step> getPtr() {
        return ptr;
    }

    public void setPtr(List<Step> ptr) {
        this.ptr = ptr;
    }
}
