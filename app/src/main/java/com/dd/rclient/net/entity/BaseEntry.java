package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/10/7.
 * Description:
 */
public class BaseEntry {

    private String token;
    private String uniSeri;
    private long buyerId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUniSeri() {
        return uniSeri;
    }

    public void setUniSeri(String uniSeri) {
        this.uniSeri = uniSeri;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }
}
