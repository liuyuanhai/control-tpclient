package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/28.
 * Description:
 */
public class LoginDataBean {

    /**
     * token : 211C091DD4FE5C96238071F1FBC88A13.C6232D0975B27E78654713D061A1309064B7AD6E2E41496EB7FEC322D9CD0D64CC8771353E85CDAFF16ADE60EF776792CC8771353E85CDAFF16ADE60EF776792
     * accountType : SUP
     * currentAuthority : SUP
     * nickName : 吴奇隆
     * mid : 12
     */

    private String token;
    private String accountType;
    private String currentAuthority;
    private String nickName;
    private long mid;
    private long buyerId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCurrentAuthority() {
        return currentAuthority;
    }

    public void setCurrentAuthority(String currentAuthority) {
        this.currentAuthority = currentAuthority;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }
}
