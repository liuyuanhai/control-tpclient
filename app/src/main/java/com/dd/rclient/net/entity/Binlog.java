package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/26.
 * Description: 登录数据接收对象
 */
public class Binlog {

    /**
     * status : 0
     * msg : 登陆成功
     */

    private int status;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
