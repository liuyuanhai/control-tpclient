package com.dd.rclient.net.map;

import com.dd.rclient.BuildConfig;
import com.dd.rclient.net.entity.Binlog;
import com.dd.rclient.net.entity.Login;
import com.dd.rclient.net.entity.Unbind;
import com.dd.rclient.net.entity.Update;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by: Administrator.
 * Created date: 2018/10/21.
 * Description: 服务器接口映射
 */
public interface ApiService {

    /* 服务器地址, 在 build.gradle 中配置 */
    String BASE_URL = BuildConfig.SERVER_BASE_API;
    /* 超时 */
    int TIME_OUT = 15;

    /**
     * 检查更新
     */
    @POST("/suspend/v1.0/app/checkUpdate")
    Call<Update> checkUpdate(
            // 版本号 x.y.z
            @Query("version") String version,
            // 客户端类型 ANDROID IOS
            @Query("appType") String appType
    );

    /**
     * 设备信息收集
     */
    @POST("/suspend/v1.0/app/pushDeviceInfo")
    Call<Object> pushDeviceInfo(
            // 设备唯一标识
            @Query("uniSeri") String uniSeri,
            // APP版本名
            @Query("versionName") String versionName,
            // APP版本号
            @Query("versionCode") String versionCode,
            // 系统版本号
            @Query("osVersion") String osVersion,
            // 系统API级别
            @Query("apiLevel") int apiLevel,
            // 设备厂商
            @Query("deviceVendor") String deviceVendor,
            // 设备型号
            @Query("deviceMode") String deviceMode,
            // CUP架构支持
            @Query("cpuAbis") String cpuAbis
    );

    /**
     * 登录
     */
    @POST("/suspend/v1.0/app/login")
    Call<Login> login(
            // 账号
            @Query("account") String account,
            // 密码
            @Query("password") String password,
            // 设备唯一标识
            @Query("uniSeri") String uniSeri
    );

    /**
     * 日志上传
     */
    @Multipart
    @POST("/suspend/v1.0/app/log")
    Call<Binlog> binlog(@Part MultipartBody.Part body);

    /**
     * 解绑设备
     */
    @POST("/suspend/v1.0/app/unBindDevice")
    Call<Unbind> unlock(
            // 设备序列号
            @Query("deviceSeri") String deviceSeri
    );
}
