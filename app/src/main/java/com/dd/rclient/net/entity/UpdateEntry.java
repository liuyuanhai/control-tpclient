package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2020/7/19.
 * Description:
 */
public class UpdateEntry {

    /**
     * message : App有新的版本发布，为了更稳定的联机，您需要更新到最新版本才能继续进行操作！
     * uri : http://didigongzuo.com/files/apks/d5d3467f-8aa4-4410-9538-af1295ce40eb.apk
     * version : 1.1.5
     */

    private String message;
    private String uri;
    private String version;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
