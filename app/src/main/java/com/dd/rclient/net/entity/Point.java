package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created at: 2022/4/13.
 * Description:
 */
public class Point {
    private int index;
    private float x;
    private float y;

    public Point() {
    }

    public Point(int index, float x, float y) {
        this.index = index;
        this.x = x;
        this.y = y;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
