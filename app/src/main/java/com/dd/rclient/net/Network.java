package com.dd.rclient.net;

import com.dd.rclient.net.interceptor.TokenInterceptor;
import com.dd.rclient.net.map.ApiService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by: Administrator.
 * Created date: 2018/10/21.
 * Description: 网络访问工具类
 */
public class Network {

    /* 网络接口 */
    private static ApiService api;
    private static OkHttpClient client;

    /**
     * 初始化方法,需要在{@link android.app.Application} 中初始化
     */
    public static void init() {
        Network.client = Network.initHttpClient();
        Network.api = Network.initApiService(Network.client);
    }

    /**
     * 初始化 {@link OkHttpClient}
     *
     * @return {@link OkHttpClient}
     */
    private static OkHttpClient initHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new TokenInterceptor())
                .connectTimeout(ApiService.TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(ApiService.TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(ApiService.TIME_OUT, TimeUnit.SECONDS).build();
    }

    /**
     * 初始化网络请求接口
     *
     * @param client {@link OkHttpClient}
     * @return {@link ApiService}
     */
    private static ApiService initApiService(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    /**
     * 获取 {@link OkHttpClient} 对象
     *
     * @return {@link OkHttpClient}
     */
    public static OkHttpClient client() {
        if (Network.client == null) {
            Network.init();
        }
        return Network.client;
    }

    /**
     * 切换请求地址
     *
     * @param url 地址
     */
    public static void replace(String url) {
        Network.api = new Retrofit.Builder()
                .baseUrl(url)
                .client(Network.client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    /**
     * 获取网路接口
     *
     * @return 返回一个ApiService接口对象
     */
    public static ApiService api() {
        return Network.api;
    }
}
