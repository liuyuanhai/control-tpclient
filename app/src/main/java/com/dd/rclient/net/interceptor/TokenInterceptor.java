package com.dd.rclient.net.interceptor;

import androidx.annotation.NonNull;

import com.dd.rclient.setting.Settings;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.utils.Text;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by: Administrator.
 * Created date: 2019/6/25.
 * Description: Token拦截器
 */
public class TokenInterceptor implements Interceptor {

    @Override
    @NonNull
    public Response intercept(@NonNull Chain chain) throws IOException {
        String token = Prefer.get(Settings.TOKEN, "");
        if (Text.empty(token)) {
            Request request = chain.request();
            return chain.proceed(request);
        } else {
            Request request = chain.request();
            Request build = request.newBuilder().header("Authorization", token).build();
            return chain.proceed(build);
        }
    }
}
