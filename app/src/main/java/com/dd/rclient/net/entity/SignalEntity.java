package com.dd.rclient.net.entity;

import com.dd.rclient.webrtc.value.AccountType;

/**
 * Created by: Administrator.
 * Created date: 2019/9/29.
 * Description: 证书
 */
public class SignalEntity {

    private String token;
    private String uniSeri;
    private String type;
    private Long buyerId;
    private AccountType accountType;
    private Payload payload;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUniSeri() {
        return uniSeri;
    }

    public void setUniSeri(String uniSeri) {
        this.uniSeri = uniSeri;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public static class Payload {

        private Long cMid;
        private Session answer;

        public Long getcMid() {
            return cMid;
        }

        public void setcMid(Long cMid) {
            this.cMid = cMid;
        }

        public Session getAnswer() {
            return answer;
        }

        public void setAnswer(Session answer) {
            this.answer = answer;
        }

        public static class Session {

            public String type;
            public String sdp;

            public Session() {
            }

            public Session(String type, String sdp) {
                this.type = type;
                this.sdp = sdp;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getSdp() {
                return sdp;
            }

            public void setSdp(String sdp) {
                this.sdp = sdp;
            }
        }
    }
}
