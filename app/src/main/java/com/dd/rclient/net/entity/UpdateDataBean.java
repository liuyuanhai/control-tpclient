package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/24.
 * Description:
 */
public class UpdateDataBean {

    /**
     * versionDesc : 修复了过于流畅的Bug，开除了一个产品经理，干掉了一个技术牛逼的程序员
     * toUpdate : true
     * version : 1.0.1
     * fileUri : https://www.w3school.com.cn/example/html5/mov_bbb.mp4
     */

    private String versionDesc;
    private boolean toUpdate;
    private String version;
    private String fileUri;

    public String getVersionDesc() {
        return versionDesc;
    }

    public void setVersionDesc(String versionDesc) {
        this.versionDesc = versionDesc;
    }

    public boolean isToUpdate() {
        return toUpdate;
    }

    public void setToUpdate(boolean toUpdate) {
        this.toUpdate = toUpdate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }
}
