package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/23.
 * Description: 检查更新数据对象
 */
public class Update {

    /**
     * status : 0
     * data : {"versionDesc":"修复了过于流畅的Bug，开除了一个产品经理，干掉了一个技术牛逼的程序员","toUpdate":true,"version":"1.0.1","fileUri":"https://www.w3school.com.cn/example/html5/mov_bbb.mp4"}
     * msg : 请求成功
     */

    private int status;
    private UpdateDataBean data;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UpdateDataBean getData() {
        return data;
    }

    public void setData(UpdateDataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
