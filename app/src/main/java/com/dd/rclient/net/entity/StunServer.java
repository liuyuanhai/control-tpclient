package com.dd.rclient.net.entity;

import java.util.List;

/**
 * Created by: Administrator.
 * Created date: 2019/9/29.
 * Description: 穿墙服务器数据对象
 */
public class StunServer {

    /**
     * initConfig : {"iceServers":[{"urls":"stun:23.21.150.121"}]}
     */

    private InitConfigBean initConfig;

    public InitConfigBean getInitConfig() {
        return initConfig;
    }

    public void setInitConfig(InitConfigBean initConfig) {
        this.initConfig = initConfig;
    }

    public static class InitConfigBean {

        private List<IceServersBean> iceServers;

        public List<IceServersBean> getIceServers() {
            return iceServers;
        }

        public void setIceServers(List<IceServersBean> iceServers) {
            this.iceServers = iceServers;
        }

        public static class IceServersBean {
            /**
             * urls : stun:23.21.150.121
             */

            private String urls;
            private String credential;
            private String username;

            public String getUrls() {
                return urls;
            }

            public void setUrls(String urls) {
                this.urls = urls;
            }

            public String getCredential() {
                return credential;
            }

            public void setCredential(String credential) {
                this.credential = credential;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }
}
