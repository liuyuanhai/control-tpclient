package com.dd.rclient.net.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/26.
 * Description: 登录数据接收对象
 */
public class Login {

    /**
     * status : 0
     * data : {"token":"211C091DD4FE5C96238071F1FBC88A13.C6232D0975B27E78654713D061A1309064B7AD6E2E41496EB7FEC322D9CD0D64CC8771353E85CDAFF16ADE60EF776792CC8771353E85CDAFF16ADE60EF776792","accountType":"SUP","currentAuthority":"SUP","nickName":"吴奇隆","mid":12}
     * msg : 登陆成功
     */

    private int status;
    private LoginDataBean data;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LoginDataBean getData() {
        return data;
    }

    public void setData(LoginDataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
