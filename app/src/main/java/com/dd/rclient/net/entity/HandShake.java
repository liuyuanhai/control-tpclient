package com.dd.rclient.net.entity;

import com.dd.rclient.webrtc.value.AccountType;

/**
 * Created by: Administrator.
 * Created date: 2019/9/28.
 * Description: 握手数据对象
 */
public class HandShake {

    private String token;
    private String uniSeri;
    private String message;
    private Long tarMid;
    private Long buyerId;
    private AccountType accountType;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTarMid() {
        return tarMid;
    }

    public void setTarMid(Long tarMid) {
        this.tarMid = tarMid;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getUniSeri() {
        return uniSeri;
    }

    public void setUniSeri(String uniSeri) {
        this.uniSeri = uniSeri;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }
}
