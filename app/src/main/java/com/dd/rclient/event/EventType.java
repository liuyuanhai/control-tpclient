package com.dd.rclient.event;

/**
 * Created by: Administrator.
 * Created date: 2019/10/2.
 * Description: 事件类型
 */
public enum EventType {
    KEY_MENU,
    KEY_HOME,
    KEY_BACK,
    KEY_WAKEUP,
    TOUCH_DOWN,
    TOUCH_MOVE,
    TOUCH_UP,
    TEXT,
    IMAGE,
    CLOSE,
}
