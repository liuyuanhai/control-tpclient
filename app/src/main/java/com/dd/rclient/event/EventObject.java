package com.dd.rclient.event;

/**
 * Created by: Administrator.
 * Created date: 2019/10/2.
 * Description: 事件对象
 */
public class EventObject {
    // 事件类型
    private EventType action;
    // 事件数据
    private Object data;

    public EventType getAction() {
        return action;
    }

    public void setAction(EventType action) {
        this.action = action;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static class Point {
        // X坐标
        private String xRatio;
        // Y坐标
        private String yRatio;

        public String getxRatio() {
            return xRatio;
        }

        public void setxRatio(String xRatio) {
            this.xRatio = xRatio;
        }

        public String getyRatio() {
            return yRatio;
        }

        public void setyRatio(String yRatio) {
            this.yRatio = yRatio;
        }
    }
}
