package com.dd.rclient.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Administrator.
 * Created date: 2019/9/22.
 * Description: 服务工具类
 */
public class WorkerService {

    /**
     * 判断服务是否正在运行
     *
     * @param context   上线问参数
     * @param className 服务类名称
     * @return true: 正在运行 false: 未在运行
     */
    public static boolean isWorked(Context context, String className) {
        ActivityManager service = (ActivityManager) context.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        if (service != null) {
            ArrayList<ActivityManager.RunningServiceInfo> services = (ArrayList<ActivityManager.RunningServiceInfo>) service.getRunningServices(Integer.MAX_VALUE);
            for (ActivityManager.RunningServiceInfo serviceInfo : services) {
                if (serviceInfo.service.getClassName().equals(className)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 将本应用置顶到最前端
     * 当本应用位于后台时，则将它切换到最前端
     */
    public static void setTopApp(Context context) {
        if (!isRunningForeground(context)) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (activityManager != null) {
                List<ActivityManager.RunningTaskInfo> taskInfoList = activityManager.getRunningTasks(100);
                for (ActivityManager.RunningTaskInfo taskInfo : taskInfoList) {
                    ComponentName top = taskInfo.topActivity;
                    if (top != null) {
                        if (top.getPackageName().equals(context.getPackageName())) {
                            activityManager.moveTaskToFront(taskInfo.id, 0);
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * 判断本应用是否已经位于最前端
     */
    private static boolean isRunningForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            List<ActivityManager.RunningAppProcessInfo> appProcessInfoList = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo appProcessInfo : appProcessInfoList) {
                if (appProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    if (appProcessInfo.processName.equals(context.getApplicationInfo().processName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
