package com.dd.rclient.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by: Administrator.
 * Created date: 2019/9/23.
 * Description: ROOT权限工具类
 */
public class SuperPermission {

    /**
     * ROOT权限检查
     *
     * @return true: 拥有root权限 false: 未拥有root权限
     */
    public static boolean hasSuperPermission() {
        return /*verifySU() &&*/ verifySuperAuth();
    }

    /**
     * 验证SU文件是否存在
     *
     * @return true: 存在 false: 不存在
     */
    private static boolean verifySU() {
        String[] files = new String[]{
                "/bin/su",
                "/xbin/su",
                "/sbin/su",
                "/system/bin/su",
                "/system/sbin/su",
                "/system/xbin/su",
                "/vendor/bin/su",
        };
        boolean isHave = false;
        for (String name : files) {
            File file = new File(name);
            if (file.exists()) {
                isHave = true;
            }
        }
        return isHave;
    }

    /**
     * ROOT授权检查
     *
     * @return true: 成功 false: 失败
     */
    private static boolean verifySuperAuth() {
        int result = -1;
        Process exec = null;
        DataOutputStream dos = null;
        try {
            exec = Runtime.getRuntime().exec("su");
            dos = new DataOutputStream(exec.getOutputStream());
            dos.writeBytes("exit\n");
            dos.flush();
            result = exec.waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (dos != null) {
                    dos.close();
                }
                if (exec != null) {
                    exec.destroy();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result == 0;
    }
}
