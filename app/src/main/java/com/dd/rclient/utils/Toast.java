package com.dd.rclient.utils;

import androidx.annotation.StringRes;

/**
 * Created by: Administrator.
 * Created time: 2019/5/22.
 * Description: 统一Toast接口
 */
public class Toast {

    public static final int LENGTH_SHORT = 0;
    public static final int LENGTH_LONG = 1;

    /**
     * 显示Toast
     *
     * @param id 字符串资源ID
     */
    public static void show(@StringRes int id) {
//        ToastView.ins.showToast(MainApplication.ins().getString(id), Gravity.CENTER, Toast.LENGTH_SHORT);
    }

    /**
     * 显示Toast
     *
     * @param content 提示信息
     */
    public static void show(String content) {
//        ToastView.ins.showToast(content, Gravity.CENTER, Toast.LENGTH_SHORT);
    }

    /**
     * 显示Toast
     *
     * @param content 提示信息
     * @param gravity 显示位置
     */
    public static void show(String content, int gravity) {
//        ToastView.ins.showToast(content, gravity, Toast.LENGTH_SHORT);
    }

    /**
     * 显示Toast
     *
     * @param content  提示信息
     * @param gravity  显示位置
     * @param duration 显示时长
     */
    public static void show(String content, int gravity, int duration) {
//        ToastView.ins.showToast(content, gravity, duration);
    }

    /**
     * 显示Toast
     *
     * @param id 字符串资源ID
     */
    public static void showSync(@StringRes int id) {
//        Activity activity = MainApplication.ins().activity();
//        if (activity != null) {
//            activity.runOnUiThread(() -> ToastView.ins.showToast(MainApplication.ins().getString(id), Gravity.CENTER, Toast.LENGTH_SHORT));
//        }
    }

    /**
     * 显示Toast
     *
     * @param content 提示信息
     */
    public static void showSync(String content) {
//        Activity activity = MainApplication.ins().activity();
//        if (activity != null) {
//            activity.runOnUiThread(() -> ToastView.ins.showToast(content, Gravity.CENTER, Toast.LENGTH_SHORT));
//        }
    }

    /**
     * 显示Toast
     *
     * @param content 提示信息
     * @param gravity 显示位置
     */
    public static void showSync(String content, int gravity) {
//        Activity activity = MainApplication.ins().activity();
//        if (activity != null) {
//            activity.runOnUiThread(() -> ToastView.ins.showToast(content, gravity, Toast.LENGTH_SHORT));
//        }
    }

    /**
     * 显示Toast
     *
     * @param content  提示信息
     * @param gravity  显示位置
     * @param duration 显示时长
     */
    public static void showSync(String content, int gravity, int duration) {
//        Activity activity = MainApplication.ins().activity();
//        if (activity != null) {
//            activity.runOnUiThread(() -> ToastView.ins.showToast(content, gravity, duration));
//        }
    }
}
