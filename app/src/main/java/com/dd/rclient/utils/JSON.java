package com.dd.rclient.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by: Administrator.
 * Created date: 2018/10/6.
 * Description: JSON序列化与反序列化工具类
 */
public class JSON {

    private static Gson gson = new Gson();

    /**
     * Object序列化为JSON
     *
     * @param object 对象
     * @return 返回序列化后的JSON字符串
     */
    public static String toJSONString(Object object) {
        return gson.toJson(object);
    }

    /**
     * 转换为JSONObject
     *
     * @param object 对象
     * @return 返回 {@link JSONObject}
     */
    public static JSONObject toJSONObject(Object object) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(JSON.toJSONString(object));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * JSON反序列化为Object
     *
     * @param json  JSON字符串
     * @param clazz 反序列化类型
     * @param <T>   泛型
     * @return 返回反序列化后的泛型对象, 反序列化异常则返回null
     */
    public static <T> T toObject(String json, Class<T> clazz) {
        T object = null;
        try {
            object = gson.fromJson(json, clazz);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }

    /**
     * JSON反序列化为Object
     *
     * @param context 上下文参数
     * @param name    资源名称
     * @param clazz   反序列化类型
     * @param <T>     泛型
     * @return 返回反序列化后的泛型对象, 反序列化异常则返回null
     */
    public static <T> T toObject(Context context, String name, Class<T> clazz) {
        T object = null;
        try {
            AssetManager assets = context.getAssets();
            if (assets != null) {
                InputStream open = assets.open(name);
                InputStreamReader isr = new InputStreamReader(open);
                object = gson.fromJson(isr, clazz);
                open.close();
                isr.close();
            }
        } catch (JsonSyntaxException | IOException e) {
            e.printStackTrace();
        }
        return object;
    }
}
