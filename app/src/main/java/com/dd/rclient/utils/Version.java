package com.dd.rclient.utils;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.dd.rclient.MainApplication;

/**
 * Created by: Administrator.
 * Created date: 2019/2/24.
 * Description: 版本相关工具类
 */
public class Version {

    /**
     * 获取应用程序版本号
     *
     * @return 返回应用程序的当前版本号字符串
     */
    public static String appVersionName() {
        String version = null;
        try {
            PackageManager pm = MainApplication.ins().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(MainApplication.ins().getPackageName(), 0);
            version = pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * 格式化版本号
     *
     * @return 返回 x.y.z
     */
    public static String cleanAppVersionName() {
        String name = Version.appVersionName();
        return name.replace("V", "")
                .replace("-debug", "")
                .replace("-release", "")
                .replace("-beta", "");
    }
}
