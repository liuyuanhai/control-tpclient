package com.dd.rclient.setting;

/**
 * Created by: Administrator.
 * Created date: 2019/9/21.
 * Description: 数据设置
 */
public class Settings {

    // 用户账户
    public final static String USER_ACCOUNT = "app.user.account";

    // 用户密码
    public final static String USER_PASSWORD = "app.user.password";

    // token
    public final static String TOKEN = "app.user.token";

    // 挂机时间
    public final static String TIME = "app.auto.time";

    // 用户ID
    public final static String TAR_MID = "app.tar.mid";

    // 昵称
    public final static String NICK_NAME = "app.nick.name";

    // 唯一标识
    public final static String SIR_NUM = "app.sir.number";

    // LocaleServerSocket
    public final static String SOCKET_ADDRESS = "app.socket.com";

    // 买号ID
    public final static String BUYER_ID = "app.buyer.id";

    // url
    public final static String URL = "app.host.url";

    // IP
    public final static String IP = "app.host.ip";

    // 屏幕宽度
    public final static String SCREEN_WIDTH = "app.screen.width";

    // 屏幕高度
    public final static String SCREEN_HEIGHT = "app.screen.height";

    // 在线状态
    public final static String ONLINE_STATE = "app.online.state";

    // 退出状态
    public final static String RUNNING_STATE = "app.exit.state";

    // 首次启动
    public final static String FIRST_START = "app.first.start";

    // 服务重启次数
    public final static String RESTART_COUNT = "app.restart.count";

    // 服务启动时间
    public final static String START_TIME = "app.start.time";

    // 设置系统
    public final static String SET_SYSTEM = "app.set.system";

    // 屏幕捕获授权状态
    public final static String SCREEN_CAP_STATE = "app.cap.state";

    // 是否具有ROOT权限
    public final static String DEVICE_HAS_ROOT = "app.state.root";

    // 启动次数
    public final static String BOOT_COUNT = "app.boot.count";

    // 系统信息
    public final static String APP_VERSION_NAME = "app.version.name";
    public final static String APP_VERSION_CODE = "app.version.code";
    public final static String APP_SYSTEM_REL = "app.system.rel";
    public final static String APP_SDK_INT = "app.sdk.int";
    public final static String APP_SYSTEM_MANUFACTURER = "app.system.man";
    public final static String APP_SYSTEM_ABI = "app.system.abi";
    public final static String APP_SYSTEM_MODEL = "app.system.model";
}
