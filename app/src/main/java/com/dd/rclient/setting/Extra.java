package com.dd.rclient.setting;

/**
 * Created by: Administrator.
 * Created date: 2019/9/23.
 * Description: 数据传递字段
 */
public class Extra {

    // 屏幕宽度
    public static final String SCREEN_WIDTH = "app.screen.width";
    // 屏幕高度
    public static final String SCREEN_HEIGHT = "app.screen.height";
    // 请求返回数据
    public static final String REQUEST_DATA = "app.media.data";
}
