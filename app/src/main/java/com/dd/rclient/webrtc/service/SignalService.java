package com.dd.rclient.webrtc.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.BitmapFactory;
import android.media.projection.MediaProjection;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.view.KeyEvent;

import androidx.annotation.Nullable;

import com.dd.rclient.BuildConfig;
import com.dd.rclient.MainApplication;
import com.dd.rclient.R;
import com.dd.rclient.daemon.AbsWorkService;
import com.dd.rclient.event.EventObject;
import com.dd.rclient.event.EventType;
import com.dd.rclient.logger.Log;
import com.dd.rclient.net.Network;
import com.dd.rclient.net.entity.HandShake;
import com.dd.rclient.net.entity.SignalEntity;
import com.dd.rclient.net.entity.StunServer;
import com.dd.rclient.net.entity.UpdateEntry;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.touch.TouchScreen;
import com.dd.rclient.ui.activity.MainActivity;
import com.dd.rclient.utils.JSON;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.utils.Text;
import com.dd.rclient.utils.Toast;
import com.dd.rclient.utils.Version;
import com.dd.rclient.webrtc.OnSocketConnectStateChangeListener;
import com.dd.rclient.webrtc.entity.IceCandidateEntity;
import com.dd.rclient.webrtc.entity.SignalActionRequest;
import com.dd.rclient.webrtc.entity.SignalActionsResponse;
import com.dd.rclient.webrtc.entity.SignalCandidateData;
import com.dd.rclient.webrtc.entity.SignalIceCandidate;
import com.dd.rclient.webrtc.entity.SignalOfferData;
import com.dd.rclient.webrtc.value.AccountType;
import com.dd.rclient.webrtc.value.SocketEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.ScreenCapturerAndroid;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by: Administrator.
 * Created date: 2019/9/14.
 * Description: 媒体处理服务
 */
public class SignalService extends AbsWorkService implements PeerConnection.Observer, SdpObserver, DataChannel.Observer {

    private static Intent data;
    private static SignalService INS;
    private final static String HOST = BuildConfig.SOCKET_IO;
    private final static String channelId = "CHANNEL_ID_MOBILE_HELPER";
    private final static int noticeId = 0x116;
    private static OnSocketConnectStateChangeListener callback;
    private Socket socket;
    private NotificationManager mNotificationManager;
    private int width;
    private int height;
    private int tx;
    private int ty;
    private String token;
    private String sirNum;
    private long tarMid;
    private long mid;
    private long buyerId;
    private TouchScreen touch;
    private PeerConnectionFactory factory;
    private MediaConstraints constraints;
    private List<PeerConnection.IceServer> servers;
    private PeerConnection connection;
    private DataChannel channel;
    private EglBase egl;
    private ScreenCapturerAndroid videoCapture;
    private boolean mecConnected = false;
    private boolean timeout = false;
    private boolean online = false;
    private boolean connecting = false;
    private boolean started = false;
    private boolean isInitialize = false;
    private long oldTouchTime;
    private PowerManager.WakeLock wakeLock;
    private int sWidth;
    private int sHeight;
    public static boolean sShouldStopService;
    public static Disposable sDisposable;
    private Disposable timer;

    @Override
    public void onCreate() {
        super.onCreate();
        SignalService.INS = this;
    }

    /**
     * 初始化
     */
    private void init() {
        this.initConfig();
        if (!isInitialize) {
            this.isInitialize = true;
            this.servers = new ArrayList<>();
            this.mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            this.constraints = new MediaConstraints();
            this.egl = EglBase.create();
            PeerConnectionFactory.InitializationOptions.Builder builder = PeerConnectionFactory.InitializationOptions.builder(getApplicationContext());
            builder.setEnableInternalTracer(true);
//            builder.setInjectableLogger((s, severity, s1) -> Logcat.write(String.format("[%s] %s -> %s", severity.name(), s, s1)), Logging.Severity.LS_WARNING);
            builder.setFieldTrials("WebRTC-H264HighProfile/Enabled/");
//            builder.setFieldTrials("WebRTC-H264Simulcast/Enabled/");
//            builder.setFieldTrials("WebRTC-SupportVP9SVC/Enabled/");
//            builder.setFieldTrials("WebRTC-SimulcastScreenshare/Enabled/");
            PeerConnectionFactory.initialize(builder.createInitializationOptions());
            this.initFactory();
        }
        this.initSocket();
        this.initNoticeChannel();
        this.initWakeLock();
    }

    /**
     * 重连
     */
    public void reconnect() {
        this.close();
        this.connect();
    }

    /**
     * 连接
     */
    public void connect() {
        Prefer.put(Settings.ONLINE_STATE, true);
        connecting = true;
        timeout = false;
        started = true;
        init();
    }

    /**
     * 关闭
     */
    public void close() {
        Prefer.put(Settings.ONLINE_STATE, false);
        release();
        connecting = false;
        started = false;
    }

    /**
     * 初始化配置参数
     */
    private void initConfig() {
        this.token = Prefer.get(Settings.TOKEN, "");
        this.sirNum = Prefer.get(Settings.SIR_NUM, "");
        this.tarMid = Prefer.get(Settings.TAR_MID, -1L);
//        this.buyerId = Prefer.get(Settings.BUYER_ID, -1L);
        this.buyerId = Prefer.get(Settings.TAR_MID, -1L);
        this.width = Prefer.get(Settings.SCREEN_WIDTH, 1080);
        this.height = Prefer.get(Settings.SCREEN_HEIGHT, 1920);
//        double ratio = 720.0 / width;
        double ratio = 540.0 / width;
//        double ratio = 485.0 / width;
        this.sWidth = (int) (width * ratio);
        this.sHeight = (int) (height * ratio);
    }

    /**
     * 是否在线
     */
    public boolean isOnline() {
        return online;
    }

    /**
     * 正在连接中
     */
    public boolean isConnecting() {
        return connecting;
    }

    public static void setData(Intent data) {
        SignalService.data = data;
    }

    public static Intent getData() {
        return data;
    }

    /**
     * 停止服务
     */
    public static void stopService() {
        //我们现在不再需要服务运行了, 将标志位置为 true
        sShouldStopService = true;
        //取消对任务的订阅
        if (sDisposable != null) sDisposable.dispose();
        //取消 Job / Alarm / Subscription
        cancelJobAlarmSub();
        SignalService.ins().destroy();
    }

    @Override
    public Boolean shouldStopService(Intent intent, int flags, int startId) {
        return sShouldStopService;
    }

    @Override
    public void startWork(Intent intent, int flags, int startId) {
        SignalService.INS = this;
        this.isInitialize = false;
        this.initConfig();
        if (Prefer.get(Settings.RUNNING_STATE, false)) {
            sDisposable = Observable
                    .interval(3, TimeUnit.SECONDS)
                    //取消任务时取消定时唤醒
                    .doOnDispose(AbsWorkService::cancelJobAlarmSub)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(count -> {
                        Log.d("Service running... count = " + count);
                        // 不是第一次采样
                        if (count > 0) {
                            // 满足5分钟
                            if (count % 100 == 0) {
                                // 控制端未连接
                                if (!mecConnected) {
                                    // 已经处于主动挂机状态
                                    if (Prefer.get(Settings.ONLINE_STATE, false)) {
                                        Log.d("SDP channel reconnect");
                                        reSdpChannel();
                                    }
                                }
                            }
                        }
                    });
        }
        if (Prefer.get(Settings.ONLINE_STATE, false)) {
            this.connect();
            startMainActivity();
            checkSystemSettings();
        }
    }

    /**
     * 提示检查系统设置
     */
    private void checkSystemSettings() {
        int count = Prefer.get(Settings.RESTART_COUNT, 0);
        if (count > 2) {
            // 计算开始挂机到服务重启中间的间隔时间
            long start = Prefer.get(Settings.START_TIME, 0L);
            long time = System.currentTimeMillis() - start;
            // 一小时内重启服务3次以上
            if (time <= 60 * 60 * 1000) {
                // 提示用户需要检查后台进程保留设置
                if (callback != null) {
                    callback.needSetSystem();
                }
            }
        } else {
            count += 1;
            Prefer.put(Settings.RESTART_COUNT, count);
        }
    }

    @Override
    public void stopWork(Intent intent, int flags, int startId) {
        stopService();
        destroy();
    }

    @Override
    public Boolean isWorkRunning(Intent intent, int flags, int startId) {
        //若还没有取消订阅, 就说明任务仍在运行.
        return sDisposable != null && !sDisposable.isDisposed();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent, Void alwaysNull) {
        return null;
    }

    @Override
    public void onServiceKilled(Intent rootIntent) {
//        SignalService.data = null;
        destroy();
    }

    /**
     * 获取服务运行状态
     */
    public static boolean isWorking() {
        return sDisposable != null && !sDisposable.isDisposed();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 释放资源
     */
    private void release() {
        destroyWebRTCConnection();
        destroyWebRTC();
        destroyNotification();
        disconnectWebSocket();
    }

    /**
     * 解决NAT机制
     * <p>
     * NAT机制:
     * 打洞的通道在一定时间内没有数据传输NAT会关闭这个洞,所以在一定时间内重新打洞,保证数据通道一直畅通
     */
    private void reSdpChannel() {
        destroyWebRTCConnection();
        destroyWebRTC();
        initWebRTC();
    }

    /**
     * 释放资源
     */
    private void destroy() {
        if (started && isInitialize) {
            if (timer != null && !timer.isDisposed()) {
                timer.dispose();
            }
            destroyWebRTCFactory();
            destroyTouchEvent();
        }
//        SignalService.data = null;
    }

    /**
     * 初始化屏幕唤醒策略
     */
    private void initWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null) {
            wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getString(R.string.app_name));
        }
    }

    /**
     * 获取连接状态
     *
     * @return true: 已连接 false: 等待连接
     */
    public boolean isConnected() {
        return this.mecConnected;
    }

    /**
     * 清除状态栏通知
     */
    private void destroyNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(noticeId);
        }
    }

    /**
     * 释放屏幕事件模拟
     */
    private void destroyTouchEvent() {
        if (touch != null) {
            touch.destroy();
        }
    }

    /**
     * 关闭WebSocket
     */
    private void disconnectWebSocket() {
        if (socket != null) {
            socket.off();
            socket.disconnect();
            socket.close();
            socket = null;
            online = false;
        }
    }

    /**
     * 销毁WebRTC
     */
    private void destroyWebRTC() {
        if (videoCapture != null) {
            if (mecConnected) {
                mecConnected = false;
                videoCapture.stopCapture();
            }
            videoCapture.dispose();
            videoCapture = null;
        }
    }

    /**
     * 销毁WebRTC
     */
    private void destroyWebRTCFactory() {
        if (factory != null) {
            factory.dispose();
            factory = null;
        }
    }

    /**
     * 关闭WebRTC连接
     */
    private void destroyWebRTCConnection() {
        if (channel != null) {
            channel.unregisterObserver();
            channel.close();
            channel.dispose();
            channel = null;
        }
        if (connection != null) {
            connection.close();
            connection.dispose();
            connection = null;
        }
    }

    /**
     * 获取当前对象的静态实例
     *
     * @return {@link SignalService}
     */
    public static SignalService ins() {
        return SignalService.INS;
    }

    /**
     * 设置回调
     *
     * @param stateChangeListener 回调接口
     */
    public static void setOnSocketConnectStateChangeListener(OnSocketConnectStateChangeListener stateChangeListener) {
        SignalService.callback = stateChangeListener;
    }

    /**
     * 创建通知
     */
    private void initNoticeChannel() {
        if (mNotificationManager != null) {
            Notification.Builder builder;
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(getString(R.string.app_name) + "运行中");
                channel.enableLights(true);
                channel.setLightColor(getResources().getColor(R.color.app_green, getTheme()));
                channel.enableVibration(false);
                mNotificationManager.createNotificationChannel(channel);
                builder = new Notification.Builder(this, channelId)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.app_name) + "运行中")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(PendingIntent.getActivity(this, 0x166, intent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S ? PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE : PendingIntent.FLAG_UPDATE_CURRENT))
                        .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
                Notification notification = builder.build();
                notification.flags |= Notification.FLAG_ONGOING_EVENT;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    startForeground(noticeId, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PROJECTION);
                } else {
                    mNotificationManager.notify(noticeId, notification);
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new Notification.Builder(this)
                        .setColor(getResources().getColor(R.color.app_green))
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.app_name) + "运行中")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(PendingIntent.getActivity(this, 0x166, intent, PendingIntent.FLAG_UPDATE_CURRENT))
                        .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
                Notification notification = builder.build();
                notification.flags |= Notification.FLAG_ONGOING_EVENT;
                mNotificationManager.notify(noticeId, notification);
            }
        } else {
            Log.e("NotificationManager is not initialization");
        }
    }

    /**
     * 初始化Socket
     */
    private void initSocket() {
        try {
            if (socket == null) {
                IO.Options options = new IO.Options();
                options.reconnection = true;
                options.transports = new String[]{WebSocket.NAME};
                if (BuildConfig.DEBUG) {
                    socket = IO.socket(Prefer.get(Settings.URL, "") + "?token=" + this.token + "&uniSeri=" + this.sirNum + "&version=" + Version.cleanAppVersionName(), options);
                } else {
                    socket = IO.socket(HOST + "?token=" + this.token + "&uniSeri=" + this.sirNum + "&version=" + Version.cleanAppVersionName(), options);
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.socket.on(Socket.EVENT_CONNECT, this::onConnect);
        this.socket.on(Socket.EVENT_CONNECT_ERROR, this::onConnectError);
        this.socket.on(Socket.EVENT_CONNECT_TIMEOUT, this::onConnectTimeout);
        this.socket.on(Socket.EVENT_DISCONNECT, this::onDisconnect);
        this.socket.on(SocketEvent.EVENT_CONNECT, this::onHappyToCon);
        this.socket.on(SocketEvent.EVENT_MESSAGE, this::onMessage);
        this.socket.on(SocketEvent.EVENT_STATE, this::onState);
        this.socket.on(SocketEvent.EVENT_ERROR, this::onError);
        this.socket.on(SocketEvent.EVENT_STATUS, this::onStatus);
        this.socket.on(SocketEvent.EVENT_SIGNAL, this::onSignal);
        this.socket.on(SocketEvent.EVENT_BUYER_ID, this::onBuyerId);
        this.socket.on(SocketEvent.EVENT_UPDATE, this::onUpdate);
        this.socket.on(SocketEvent.EVENT_RESET, this::onBusy);
        this.socket.on(Socket.EVENT_PING, args -> System.out.println("PING: " + JSON.toJSONString(args)));
        this.socket.on(Socket.EVENT_PONG, args -> System.out.println("PONG: " + JSON.toJSONString(args)));
        this.startConnect();
    }

    /**
     * 开始屏幕捕获
     */
    private void startScreenCapture() {
        if (videoCapture != null && !mecConnected) {
            mecConnected = true;
            try {
                videoCapture.startCapture(sWidth, sHeight, 30);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.touch == null) {
            this.touch = new TouchScreen();
        }
        if (callback != null) {
            callback.onMECConnectState(true);
        }
    }

    /**
     * 停止屏幕捕获
     */
    private void stopScreenCapture() {
        if (videoCapture != null && mecConnected) {
            mecConnected = false;
            videoCapture.stopCapture();
        }
        if (this.touch != null) {
            this.touch.destroy();
            this.touch = null;
        }
        if (callback != null) {
            callback.onMECConnectState(false);
        }
    }

    /**
     * 下载图片
     *
     * @param url 图片地址
     */
    private void download(String url) {
        Log.d(url);
        OkHttpClient client = Network.client();
        Request request = new Request.Builder().get().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
                Toast.showSync("文件保存失败");
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                ResponseBody body = response.body();
                if (body != null) {
                    byte[] bytes = body.bytes();
                    byteDataToFile(bytes);
                    return;
                }
                Toast.showSync("文件保存失败");
            }
        });
    }

    /**
     * 买号ID交换
     *
     * @param objects 数据对象数组
     */
    private void onBuyerId(Object... objects) {
        Log.d("Socket Buyer Id: " + JSON.toJSONString(objects));
        try {
            JSONObject payload = ((JSONObject) objects[0]);
            this.buyerId = payload.getLong("data");
            Prefer.put(Settings.BUYER_ID, buyerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 需要更新
     *
     * @param objects 数据对象数组
     */
    private void onUpdate(Object... objects) {
        Log.d("Socket APP Need Update: " + JSON.toJSONString(objects));
        UpdateEntry update = JSON.toObject(objects[0].toString(), UpdateEntry.class);
        if (callback != null) {
            callback.needUpdate(update.getMessage(), update.getUri(), update.getVersion());
        }
    }

    /**
     * 服务器繁忙,需要重连
     *
     * @param objects 数据对象数组
     */
    private void onBusy(Object... objects) {
        Log.d("System busy: " + JSON.toJSONString(objects));
        if (callback != null) {
            callback.onBusy();
        }
    }

    /**
     * 断开链接
     *
     * @param objects 数据对象数组
     */
    private void onDisconnect(Object... objects) {
        Log.d("Socket disconnect: " + JSON.toJSONString(objects));
        online = false;
        connecting = true;
        if (callback != null && !timeout) {
            callback.onDisconnect();
        }
        destroyWebRTC();
    }

    /**
     * 链接错误
     *
     * @param objects 数据对象数组
     */
    private void onConnectError(Object... objects) {
        Log.d("Socket connect error: " + JSON.toJSONString(objects));
        online = false;
        connecting = true;
        if (callback != null) {
            callback.onConnectError();
        }
        destroyWebRTC();
    }

    /**
     * 链接超时
     *
     * @param objects 数据对象数组
     */
    private void onConnectTimeout(Object... objects) {
        Log.d("Socket connect timeout: " + JSON.toJSONString(objects));
        online = false;
        connecting = true;
        if (callback != null) {
            callback.onConnectTimeout();
        }
        destroyWebRTC();
    }

    /**
     * 创建链接
     *
     * @param objects 数据对象数组
     */
    private void onHappyToCon(Object... objects) {
        Log.d("Socket hand shake success");
        String payload = (String) objects[0];
        StunServer stunServer = JSON.toObject(payload, StunServer.class);
        initIceServers(stunServer);
        if (callback != null) {
            callback.onConnected();
        }
        // 等待授权回环
        while (data == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                // 异常了不要继续等
                break;
            }
        }
        // 联机信息准备完成，即开始创建本地PeerConnection
//        try {
//            // 方案一
//            this.initWebRTC();
//        } catch (Exception e) {
//            e.printStackTrace();
        // 仅OPPO特殊处理
        if ((Build.MANUFACTURER != null && Build.MANUFACTURER.toUpperCase().contains("OPPO"))
                || (Build.MODEL != null && Build.MODEL.toUpperCase().contains("OPPO"))) {
            // 方案二
            MainApplication application = MainApplication.ins();
            if (application != null) {
                Activity activity = application.activity();
                if (activity != null) {
                    activity.runOnUiThread(this::initWebRTC);
                }
            }

        } else {
            // 非OPPO正常处理
            this.initWebRTC();
        }
//        }
    }

    /**
     * 建立链接
     *
     * @param objects 数据对象数组
     */
    private void onConnect(Object... objects) {
        Log.d("connected to server: " + JSON.toJSONString(objects));
        online = true;
        connecting = false;
        HandShake payload = new HandShake();
        payload.setMessage("Shake!Shake!");
        payload.setTarMid(tarMid);
        payload.setBuyerId(buyerId);
        payload.setAccountType(AccountType.SUP);
        if (socket != null) {
            socket.emit(SocketEvent.EVENT_SHAKE, JSON.toJSONObject(payload));
            Log.d("send hand shake data");
        }
    }

    /**
     * 登录状态错误
     *
     * @param objects 数据对象数组
     */
    private void onState(Object... objects) {
        timeout = true;
        Log.d("Receive onState action: " + JSON.toJSONString(objects));
        if (callback != null) {
            callback.onAuthStateError(objects[0].toString());
        }
    }

    /**
     * 系统繁忙
     *
     * @param objects 数据对象数组
     */
    private void onError(Object... objects) {
        timeout = true;
        Log.d("Receive onError action: " + JSON.toJSONString(objects));
        if (callback != null) {
            callback.onSystemError(objects[0].toString());
        }
    }

    /**
     * 消息
     *
     * @param objects 数据对象数组
     */
    private void onMessage(Object... objects) {
        Log.d("Receive onMessage action: " + JSON.toJSONString(objects));
    }

    /**
     * 用户状态错误
     *
     * @param objects 数据对象数组
     */
    private void onStatus(Object... objects) {
        Log.d("Receive onStatus action: " + JSON.toJSONString(objects));
    }

    /**
     * 信令监听处理
     *
     * @param objects 数据对象数组
     */
    private void onSignal(Object... objects) {
        JSONObject jsonObject = (JSONObject) objects[0];
        String s = jsonObject.toString();
        SignalActionsResponse response = JSON.toObject(s, SignalActionsResponse.class);

        switch (response.getSignal()) {
            case "offer":
                handSignalOffer(response.getSignalData());
                break;
            case "answer":
                handSignalAnswer(response.getSignalData());
                break;
            case "candidate":
                handSignalCandidate(response.getSignalData());
                break;
        }
    }

    /**
     * 处理offer信令
     *
     * @param payload {@link JSONObject}
     */
    private void handSignalOffer(Object payload) {
        SignalOfferData offerData = JSON.toObject(JSON.toJSONString(payload), SignalOfferData.class);
        this.mid = offerData.getMid();
        SessionDescription sdp = new SessionDescription(SessionDescription.Type.OFFER, offerData.getSdp());
        if (connection != null) {
            connection.setRemoteDescription(this, sdp);
            connection.createAnswer(this, constraints);
        }
        startScreenCapture();
    }

    /**
     * 处理answer信令
     *
     * @param payload {@link JSONObject}
     */
    private void handSignalAnswer(Object payload) {
        Log.d("Hand answer signal: " + JSON.toJSONString(payload));
    }

    /**
     * 处理candidate信令
     *
     * @param payload {@link JSONObject}
     */
    private void handSignalCandidate(Object payload) {
        if (payload != null) {
            SignalCandidateData signalCandidateData = JSON.toObject(JSON.toJSONString(payload), SignalCandidateData.class);
            if (connection.getRemoteDescription() != null && signalCandidateData.getSdp() != null) {
                connection.addIceCandidate(signalCandidateData.getSdp());
            }
        }
    }

    /**
     * 初始化WebRTC
     */
    private void initWebRTC() {
        PeerConnection.RTCConfiguration configuration = new PeerConnection.RTCConfiguration(servers);
        this.connection = factory.createPeerConnection(configuration, this);
        if (this.connection != null) {
//            this.connection.setBitrate(114100, 114100, 114100);
            initMediaStream();
            DataChannel.Init init = new DataChannel.Init();
            init.ordered = true;
            this.channel = connection.createDataChannel("data_channel", init);
        }
    }

    /**
     * 初始化媒体流
     */
    private void initMediaStream() {
        MediaStream lsm = factory.createLocalMediaStream("ARDAMS");
        VideoSource videoSource = factory.createVideoSource(true, false);
        videoCapture = new ScreenCapturerAndroid(data, new MediaProjection.Callback() {
            @Override
            public void onStop() {
                super.onStop();
            }
        });

        // EGL初始化检查
//        if (egl != null) {
//            egl.releaseSurface();
//            egl.release();
//        }
//        egl = EglBase.create();

        videoCapture.initialize(SurfaceTextureHelper.create("surface_helper", egl.getEglBaseContext(), true), getApplicationContext(), videoSource.getCapturerObserver());
        VideoTrack videoTrack = factory.createVideoTrack("ARDAMSv0", videoSource);
        videoSource.adaptOutputFormat(sWidth, sHeight, 30);
        videoTrack.setEnabled(true);
        lsm.addTrack(videoTrack);
        connection.addTrack(videoTrack);
        connection.addStream(lsm);
    }

    /**
     * 初始化工厂
     */
    private void initFactory() {
        if (factory == null) {
            PeerConnectionFactory.Builder builder = PeerConnectionFactory.builder();
            builder.setVideoDecoderFactory(new DefaultVideoDecoderFactory(egl.getEglBaseContext()));
            builder.setVideoEncoderFactory(new DefaultVideoEncoderFactory(egl.getEglBaseContext(), true, true));
            factory = builder.createPeerConnectionFactory();
        }
    }

    /**
     * 开始连接
     */
    private void startConnect() {
        if (socket != null && !online) {
            socket.connect();
        }
        if (callback != null) {
            callback.onStartConnect();
        }
    }

    /**
     * 创建STUN服务器列表
     */
    public void initIceServers(StunServer stun) {
        if (stun != null) {
            StunServer.InitConfigBean config = stun.getInitConfig();
            if (config != null) {
                List<StunServer.InitConfigBean.IceServersBean> iceServers = config.getIceServers();
                if (iceServers != null && !iceServers.isEmpty()) {
                    for (StunServer.InitConfigBean.IceServersBean iceServer : iceServers) {
                        String username = iceServer.getUsername();
                        String credential = iceServer.getCredential();
                        if (!Text.empty(username) && !Text.empty(credential)) {
                            servers.add(PeerConnection.IceServer.builder(iceServer.getUrls())
                                    .setUsername(username)
                                    .setPassword(credential)
                                    .createIceServer()
                            );
                        } else {
                            servers.add(PeerConnection.IceServer.builder(iceServer.getUrls()).createIceServer());
                        }
                    }
                }
            }
        }
    }

    /*----------------------------- PeerConnection.Observer ------------------------------*/

    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {
        Log.d("method onSignalingChange: " + signalingState.name());
    }

    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        Log.d("method onIceConnectionChange: " + iceConnectionState.name());
        if (iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED) {
            mecClosed();
        }
    }

    @Override
    public void onIceConnectionReceivingChange(boolean b) {
        Log.d("method onIceConnectionReceivingChange: " + b);
    }

    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        Log.d("method onIceGatheringChange: " + iceGatheringState.name());
    }

    @Override
    public void onIceCandidate(IceCandidate iceCandidate) {
        Log.d("method onIceCandidate: " + JSON.toJSONString(iceCandidate));
        SignalActionRequest request = new SignalActionRequest();
        request.setBuyerId(buyerId);
        request.setType("CANDIDATE");
        SignalIceCandidate payload = new SignalIceCandidate();
        payload.setMid(this.mid);
        IceCandidateEntity iceCandidateEntity = new IceCandidateEntity();
        iceCandidateEntity.setCandidate(iceCandidate.sdp);
        iceCandidateEntity.setSdpMid(iceCandidate.sdpMid);
        iceCandidateEntity.setSdpMLineIndex(iceCandidate.sdpMLineIndex);
        payload.setCandidate(iceCandidateEntity);
        request.setPayload(payload);
        Log.d("Send iceCandidate to MEC: " + JSON.toJSONString(request));
        socket.emit(SocketEvent.EVENT_SIGNAL, JSON.toJSONObject(request));
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
        Log.d("method onIceCandidatesRemoved: " + JSON.toJSONString(iceCandidates));
    }

    @Override
    public void onAddStream(MediaStream mediaStream) {
        Log.d("method onAddStream: " + mediaStream.toString());
    }

    @Override
    public void onRemoveStream(MediaStream mediaStream) {
        Log.d("method onRemoveStream: " + mediaStream.toString());
    }

    @Override
    public void onDataChannel(DataChannel dataChannel) {
        Log.d("Remote dataChannel connected");
        dataChannel.registerObserver(this);
    }

    @Override
    public void onRenegotiationNeeded() {
        Log.d("method onRenegotiationNeeded");
    }

    @Override
    public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
        Log.d("method onAddTrack: ");
    }

    /* -------------------------- SdpObserver --------------------------- */

    @Override
    public void onCreateSuccess(SessionDescription sessionDescription) {
        if (connection != null) {
            connection.setLocalDescription(this, sessionDescription);
        }
        Log.d("SessionDescription create success: " + JSON.toJSONString(sessionDescription));
        SignalEntity.Payload payload = new SignalEntity.Payload();
        payload.setcMid(mid);
        payload.setAnswer(new SignalEntity.Payload.Session(sessionDescription.type.canonicalForm(), sessionDescription.description));
        SignalEntity entity = new SignalEntity();
        entity.setBuyerId(buyerId);
        entity.setAccountType(AccountType.SUP);
        entity.setType("ANSWER");
        entity.setPayload(payload);
        if (socket != null) {
            socket.emit(SocketEvent.EVENT_SIGNAL, JSON.toJSONObject(entity));
        }
    }

    @Override
    public void onSetSuccess() {
        Log.d("Set remote/local sessionDescription success");
    }

    @Override
    public void onCreateFailure(String s) {
        Log.e("Create offer/answer failure: " + s);
    }

    @Override
    public void onSetFailure(String s) {
        Log.d("Set remote/local sessionDescription failure: " + s);
    }

    /* ------------------------------- DataChannel.Observer --------------------------------- */

    @Override
    public void onBufferedAmountChange(long l) {
        Log.d("Receive buffer size: " + l);
    }

    @Override
    public void onStateChange() {

    }

    @Override
    public void onMessage(DataChannel.Buffer buffer) {
        ByteBuffer data = buffer.data;
        byte[] bytes = new byte[data.capacity()];
        data.get(bytes);
        String event = new String(bytes);
        EventObject object = JSON.toObject(event, EventObject.class);
        handleInputEvent(object);
        Log.d("Receive message: " + event);
    }

    /**
     * 事件处理
     *
     * @param event {@link EventObject}
     */
    private synchronized void handleInputEvent(EventObject event) {
        if (event != null && touch != null) {
            EventType action = event.getAction();
            if (action != null) {
                switch (action) {
                    case KEY_WAKEUP:
                        if (wakeLock != null) {
                            wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);
                            wakeLock.release();
                        } else {
                            touch.injectKeyCode(KeyEvent.KEYCODE_POWER);
                        }
                        break;
                    case KEY_MENU:
                        touch.injectKeyCode(KeyEvent.KEYCODE_APP_SWITCH);
                        break;
                    case KEY_HOME:
                        touch.injectKeyCode(KeyEvent.KEYCODE_HOME);
                        break;
                    case KEY_BACK:
                        touch.injectKeyCode(KeyEvent.KEYCODE_BACK);
                        break;
                    case TEXT:
                        touch.injectStringText(this, (String) event.getData());
                        break;
                    case IMAGE:
                        download((String) event.getData());
                        break;
                    case TOUCH_DOWN:
                        asPointer(event.getData().toString());
                        touch.injectMotionDown(tx, ty);
                        break;
                    case TOUCH_MOVE:
                        long nowTouchTime = System.currentTimeMillis();
                        if ((nowTouchTime - oldTouchTime) > 20) {
                            oldTouchTime = nowTouchTime;
                            asPointer(event.getData().toString());
                            touch.injectMotionMove(tx, ty);
                        }
                        break;
                    case TOUCH_UP:
                        asPointer(event.getData().toString());
                        touch.injectMotionUp(tx, ty);
                        break;
                    case CLOSE:
                        mecClosed();
                        break;
                    default:
                        Log.e("Not support event type");
                }
            }
        }
    }

    /**
     * 控制端主动断开
     */
    private void mecClosed() {
        startMainActivity();
        stopScreenCapture();
        // 每次断开后间隔一定时间重新打洞
        delayReSdpChannel();
    }

    /**
     * 延时重连
     */
    private void delayReSdpChannel() {
        if (timer != null && !timer.isDisposed()) {
            timer.dispose();
        }
        timer = Observable.timer(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    reSdpChannel();
                    if (timer != null && !timer.isDisposed()) {
                        timer.dispose();
                    }
                });
    }

    /**
     * 启动主界面
     */
    private void startMainActivity() {
//        if (WorkerService.isWorked(this, MainActivity.class.getName())) {
//            startActivity(new Intent(SignalService.this, MainActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
//        } else {
//            startActivity(new Intent(SignalService.this, MainActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
//        }
//        WorkerService.setTopApp(this);
    }

    /**
     * 保存到文件
     *
     * @param data 文件数据
     */
    @SuppressLint("SdCardPath")
    private void byteDataToFile(byte[] data) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        String format = ".jpg";
        String type = options.outMimeType;
        if ("image/png".equals(type)) {
            format = ".png";
        } else if ("image/jpeg".equals(type)) {
            format = ".jpg";
        } else if ("image/gif".equals(type)) {
            format = ".gif";
        }
        FileOutputStream fos = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/" + System.currentTimeMillis() + format);
            if (checkDirs(file)) {
                fos = new FileOutputStream(file);
                fos.write(data);
                fos.flush();
                Toast.showSync(String.format("文件已保存到%s", file.getAbsolutePath()));
            } else {
                Toast.showSync("文件保存失败");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 检查文件是否存在
     *
     * @param file 文件对象
     * @return true: 检查并创建成功 false: 检查并创建是失败
     * @throws IOException 可能出现的异常
     */
    private boolean checkFile(File file) throws IOException {
        if (!file.exists()) {
            if (file.createNewFile()) {
                return true;
            } else {
                Log.e("file [" + file.getAbsolutePath() + "] create fail");
            }
        }
        return false;
    }

    /**
     * 检查文件夹是否存在
     *
     * @param file 文件对象
     * @return true: 检查并创建成功 false: 检查并创建失败
     * @throws IOException 可能出现的异常
     */
    private boolean checkDirs(File file) throws IOException {
        File dir = file.getParentFile();
        if (dir != null) {
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    return checkFile(file);
                } else {
                    Log.e("directory [" + file.getAbsolutePath() + "] create fail");
                }
            } else {
                return checkFile(file);
            }
        }
        return false;
    }

    /**
     * 获取XY坐标
     *
     * @param data 数据
     */
    private synchronized void asPointer(String data) {
        EventObject.Point point = JSON.toObject(data, EventObject.Point.class);
        double xRatio = Double.parseDouble(point.getxRatio());
        double yRatio = Double.parseDouble(point.getyRatio());
        tx = (int) (xRatio * width);
        ty = (int) (yRatio * height);
    }
}
