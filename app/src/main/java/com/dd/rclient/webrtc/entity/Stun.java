package com.dd.rclient.webrtc.entity;

import java.util.List;

/**
 * Created by: Administrator.
 * Created date: 2019/9/6.
 * Description: 打洞服务器地址数据对象
 */
public class Stun {

    /**
     * version : 101
     * urls : ["stun:stun1.l.google.com:19302","stun:stun2.l.google.com:19302","stun:stun3.l.google.com:19302","stun:stun4.l.google.com:19302","stun:23.21.150.121","stun:stun01.sipphone.com","stun:stun.ekiga.net","stun:stun.fwdnet.net","stun:stun.ideasip.com","stun:stun.iptel.org","stun:stun.rixtelecom.se","stun:stun.schlund.de","stun:stunserver.org","stun:stun.softjoys.com","stun:stun.voiparound.com","stun:stun.voipbuster.com","stun:stun.voipstunt.com","stun:stun.voxgratia.org","stun:stun.xten.com"]
     */

    // 数据版本号
    private int version;
    // 服务器数据地址
    private List<String> urls;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }
}
