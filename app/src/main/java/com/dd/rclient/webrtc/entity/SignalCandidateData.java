package com.dd.rclient.webrtc.entity;

import com.dd.rclient.utils.JSON;
import com.dd.rclient.utils.Text;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;

public class SignalCandidateData {

    private Long mid;
    private Object candidate;

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Object getCandidate() {
        return candidate;
    }

    public void setCandidate(Object candidate) {
        this.candidate = candidate;
    }

    public SignalCandidateData(Long mid, IceCandidate candidate) {
        this.mid = mid;
        this.candidate = candidate;
    }

    public SignalCandidateData() {
    }

    public IceCandidate getSdp() {
        if (candidate != null) {
            JSONObject payload = JSON.toJSONObject(candidate);
            try {
                String sdpMid = payload.getString("sdpMid");
                int sdpMLineIndex = payload.getInt("sdpMLineIndex");
                String sdp = payload.getString("candidate");
                if (Text.empty(sdp)) {
                    return null;
                } else {
                    return new IceCandidate(sdpMid, sdpMLineIndex, sdp);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
