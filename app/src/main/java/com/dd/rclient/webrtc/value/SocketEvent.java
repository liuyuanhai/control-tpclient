package com.dd.rclient.webrtc.value;

/**
 * Created by: Administrator.
 * Created date: 2019/9/21.
 * Description:
 */
public class SocketEvent {

    // 握手动作
    public final static String EVENT_SHAKE = "handShake";

    // 连接动作
    public final static String EVENT_CONNECT = "happyToCon";

    // 信令
    public final static String EVENT_SIGNAL = "signal";

    // 建立链接
    public final static String EVENT_CONNECTED = "connected";

    // 登录状态错误
    public final static String EVENT_STATE = "authState";

    // 发送消息
    public final static String EVENT_MESSAGE = "pushMessage";

    // 用户状态错误
    public final static String EVENT_STATUS = "nonStatus";

    // 广播
    public final static String EVENT_BROADCAST = "broadcast";

    // 文件
    public final static String EVENT_FILE = "fileImg";

    // 主动断开
    public final static String EVENT_CLOSE = "playedOff";

    // 买号ID交换
    public final static String EVENT_BUYER_ID = "prePushLinkId";

    // 系统繁忙
    public final static String EVENT_ERROR = "error";

    // 强制下线
    public final static String EVENT_OFFLINE = "kickOut";

    // 需要更新
    public final static String EVENT_UPDATE = "needUpdate";

    // 通知重连
    public final static String EVENT_RESET = "busyError";
}
