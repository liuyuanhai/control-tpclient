package com.dd.rclient.webrtc;

/**
 * Created by: Administrator.
 * Created date: 2019/9/28.
 * Description: Socket链接状态改变监听
 */
public interface OnSocketConnectStateChangeListener {

    /**
     * 开始连接
     */
    void onStartConnect();

    /**
     * 已建立链接
     */
    void onConnected();

    /**
     * 链接错误
     */
    void onConnectError();

    /**
     * 链接超时
     */
    void onConnectTimeout();

    /**
     * 断开链接
     */
    void onDisconnect();

    /**
     * 控制端连接状态
     *
     * @param connected true: 控制端已连接 false: 等待控制端连接
     */
    void onMECConnectState(boolean connected);

    /**
     * 登录状态错误
     */
    void onAuthStateError(String text);

    /**
     * 系统繁忙
     */
    void onSystemError(String text);

    /**
     * 一小时内重连次数超过3次
     */
    void needSetSystem();

    /**
     * 需要更新
     *
     * @param message 新版本特性
     * @param url     更新地址
     * @param version 版本号
     */
    void needUpdate(String message, String url, String version);

    /**
     * 系统繁忙,等待重连
     */
    void onBusy();
}
