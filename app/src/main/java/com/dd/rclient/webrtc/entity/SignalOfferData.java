package com.dd.rclient.webrtc.entity;

import com.dd.rclient.utils.JSON;

import org.json.JSONException;
import org.json.JSONObject;

public class SignalOfferData {
    private Long mid;
    private Object offer;

    public SignalOfferData(Long mid, String offer) {
        this.mid = mid;
        this.offer = offer;
    }

    public SignalOfferData() {
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Object getOffer() {
        return offer;
    }

    public void setOffer(Object offer) {
        this.offer = offer;
    }

    public String getSdp() {
        JSONObject jsonObject = JSON.toJSONObject(this.getOffer());
        try {
            return jsonObject.getString("sdp");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
