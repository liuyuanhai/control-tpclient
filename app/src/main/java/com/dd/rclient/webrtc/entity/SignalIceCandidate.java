package com.dd.rclient.webrtc.entity;

public class SignalIceCandidate {
    private Long mid;

    private Object candidate;

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Object getCandidate() {
        return candidate;
    }

    public void setCandidate(Object candidate) {
        this.candidate = candidate;
    }

    public SignalIceCandidate(Long mid, Object candidate) {
        this.mid = mid;
        this.candidate = candidate;
    }

    public SignalIceCandidate() {
    }
}
