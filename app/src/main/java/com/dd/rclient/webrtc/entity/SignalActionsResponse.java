package com.dd.rclient.webrtc.entity;

/**
 * @author Administrator
 * Description:
 * Createtime： 2019-08-07 14:04
 * Copyright: Copyright (C) 2019 All Rights Reserved.
 */

public class SignalActionsResponse {

    private String signal;
    /**
     * 联机ID
     */
    private Object signalData;

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public Object getSignalData() {
        return signalData;
    }

    public void setSignalData(Object signalData) {
        this.signalData = signalData;
    }

    public SignalActionsResponse(String signal, Object signalData) {
        this.signal = signal;
        this.signalData = signalData;
    }

    public SignalActionsResponse() {
    }
}
