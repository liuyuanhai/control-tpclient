package com.dd.rclient.webrtc.value;

/**
 * Created by: Administrator.
 * Created date: 2019/9/28.
 * Description: 用户端类型
 */
public enum AccountType {
    SUP,
    MEC,
}