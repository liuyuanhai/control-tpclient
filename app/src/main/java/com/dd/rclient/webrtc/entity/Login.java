package com.dd.rclient.webrtc.entity;

/**
 * Created by: Administrator.
 * Created date: 2019/9/21.
 * Description: 登录数据对象
 */
public class Login {

    private String account;
    private String password;

    public Login() {
    }

    public Login(String account, String password) {
        this.account = account;
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
