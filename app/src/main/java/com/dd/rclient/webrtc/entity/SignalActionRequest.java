package com.dd.rclient.webrtc.entity;

public class SignalActionRequest {

    private String token;

    private String uniSeri;

    private String type;

    private Long buyerId;

    private Object payload;

    public SignalActionRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUniSeri() {
        return uniSeri;
    }

    public void setUniSeri(String uniSeri) {
        this.uniSeri = uniSeri;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }
}
