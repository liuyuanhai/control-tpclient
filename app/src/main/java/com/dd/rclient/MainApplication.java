package com.dd.rclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDexApplication;

import com.dd.rclient.daemon.DaemonEnv;
import com.dd.rclient.exceptions.crash.GlobalCrashCapture;
import com.dd.rclient.net.Network;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.threads.Worker;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.webrtc.service.SignalService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Administrator.
 * Created date: 2018/10/16.
 * Description: 程序入口点
 */
public class MainApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {

    /* 静态实例 */
    @SuppressLint("StaticFieldLeak")
    private static MainApplication instance;
    /* 当前所在的界面 */
    private Activity activity;
    /* Activity管理集合 */
    private List<Activity> activities;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        // 初始化全局异常捕获
        GlobalCrashCapture.instance().init(this);
        // 创建Activity储存管理
        activities = new ArrayList<>();
        // 注册Activity生命周期回调
        registerActivityLifecycleCallbacks(this);
        // 初始化线程池
        Worker.init(Worker.newCachedThreadPool());
        // 初始化网络请求
        Network.init();
        // 进程保活
        DaemonEnv.initialize(this, SignalService.class, DaemonEnv.DEFAULT_WAKE_UP_INTERVAL);
    }

    /**
     * 获取静态实例
     *
     * @return 返回一个MassApplication对象
     */
    public static MainApplication ins() {
        return instance;
    }

    /**
     * 获取当前显示的Activity
     *
     * @return 返回一个 {@link Activity} 对象
     */
    public Activity activity() {
        return this.activity;
    }

    /**
     * 退出APP
     */
    public void quit() {
        Prefer.put(Settings.RUNNING_STATE, false);
        Map<Class<? extends Service>, ServiceConnection> bind = DaemonEnv.BIND_STATE_MAP;
        Set<Map.Entry<Class<? extends Service>, ServiceConnection>> entries = bind.entrySet();
        for (Map.Entry<Class<? extends Service>, ServiceConnection> entry : entries) {
            unbindService(entry.getValue());
        }
        DaemonEnv.BIND_STATE_MAP.clear();
        for (Activity act : activities) {
            if (act != null && !act.isFinishing()) {
                act.finish();
            }
        }
        activities.clear();
        Worker.destroy();
        System.exit(0);
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, Bundle savedInstanceState) {
        activities.add(activity);
        this.activity = activity;
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @Nullable Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        activities.remove(activity);
    }
}
