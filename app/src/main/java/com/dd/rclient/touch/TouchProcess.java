package com.dd.rclient.touch;

import android.app.Instrumentation;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Looper;
import android.os.SystemClock;
import android.view.MotionEvent;

import com.dd.rclient.setting.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by: Administrator.
 * Created date: 2019/10/7.
 * Description: 屏幕触控进程
 */
public class TouchProcess {

    // 屏幕事件 按下
    private final static String EVENT_TOUCH_DOWN = "DOWN";
    // 屏幕事件 移动
    private final static String EVENT_TOUCH_MOVE = "MOVE";
    // 屏幕事件 抬起
    private final static String EVENT_TOUCH_UP = "UP";
    // 按钮事件 按键
    private final static String EVENT_KEY = "KEY";
    // 输入事件 文本
    private final static String EVENT_TEXT = "TEXT";
    // 关闭事件 结束
    private final static String EVENT_CLOSE = "CLOSE";
    // 事件处理
    private static Instrumentation inst;

    public static void main(String... args) throws IOException {
        Looper.prepare();
        inst = new Instrumentation();
        LocalSocket socket = new LocalSocket();
        socket.connect(new LocalSocketAddress(Settings.SOCKET_ADDRESS));
        InputStream is = socket.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String temp;
        while ((temp = br.readLine()) != null && !temp.equals(EVENT_CLOSE)) {
            String[] data = temp.split(":");
            handleEvent(data);
        }
        is.close();
        isr.close();
        br.close();
        socket.close();
        socket.close();
        Looper looper = Looper.myLooper();
        if (looper != null) {
            looper.quit();
        }
        Looper.loop();
    }

    /**
     * 处理事件
     *
     * @param args 事件数据
     */
    private static void handleEvent(String... args) {
        switch (args[0]) {
            case EVENT_TOUCH_DOWN:
                inst.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, Float.parseFloat(args[1]), Float.parseFloat(args[2]), 0));
                break;
            case EVENT_TOUCH_MOVE:
                inst.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, Float.parseFloat(args[1]), Float.parseFloat(args[2]), 0));
                break;
            case EVENT_TOUCH_UP:
                inst.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, Float.parseFloat(args[1]), Float.parseFloat(args[2]), 0));
                break;
            case EVENT_KEY:
                inst.sendKeyDownUpSync(Integer.parseInt(args[1]));
                break;
            case EVENT_TEXT:
                inst.sendStringSync(args[1]);
                break;
        }
    }
}
