package com.dd.rclient.touch;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Path;
import android.os.Build;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.dd.rclient.MainApplication;
import com.dd.rclient.net.entity.NativePtr;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.setting.Values;
import com.dd.rclient.threads.Worker;
import com.dd.rclient.utils.JSON;
import com.dd.rclient.utils.Prefer;
import com.dd.rclient.utils.Toast;

/**
 * Created by: Administrator.
 * Created date: 2019/10/7.
 * Description: 屏幕操作
 */
public class TouchScreen {

    private final boolean hasRoot;
    private Shell shell;
    // 屏幕事件 按下
    private final static String EVENT_TOUCH_DOWN = "DOWN";
    // 屏幕事件 移动
    private final static String EVENT_TOUCH_MOVE = "MOVE";
    // 屏幕事件 抬起
    private final static String EVENT_TOUCH_UP = "UP";
    // 按钮事件 按键
    private final static String EVENT_KEY = "KEY";
    private boolean touch = false;
    private boolean moved = false;
    private Path path = new Path();

    /**
     * 初始化
     */
    public TouchScreen() {
        hasRoot = Prefer.get(Settings.DEVICE_HAS_ROOT, false);
        if (hasRoot) {
            this.shell = new Shell();
        }
    }

    /**
     * 按下
     *
     * @param x X坐标
     * @param y Y坐标
     */
    public void injectMotionDown(int x, int y) {
        if (hasRoot) {
            this.shell.sendTouchEvent(EVENT_TOUCH_DOWN, x, y);
        } else {
            touch = true;
            moved = false;
            path.reset();
            path.moveTo(x, y);
        }
    }

    /**
     * 移动
     *
     * @param x X坐标
     * @param y Y坐标
     */
    public void injectMotionMove(int x, int y) {
        if (hasRoot) {
            this.shell.sendTouchEvent(EVENT_TOUCH_MOVE, x, y);
        } else {
            moved = true;
            path.lineTo(x, y);
        }
    }

    /**
     * 抬起
     *
     * @param x X坐标
     * @param y Y坐标
     */
    public void injectMotionUp(int x, int y) {
        if (hasRoot) {
            this.shell.sendTouchEvent(EVENT_TOUCH_UP, x, y);
        } else {
            if (moved) {
                path.lineTo(x, y);
            }
            injectMotionEvent(path, moved);
            touch = false;
            moved = false;
        }
    }

    /**
     * 无障碍手势操作
     *
     * @param path 点击位置
     * @param move 是否滑动
     */
    private void injectMotionEvent(Path path, boolean move) {
        if (Values.accessibility == null) {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            GestureDescription.Builder builder = new GestureDescription.Builder();
            GestureDescription description = builder.addStroke(new GestureDescription.StrokeDescription(path, 0, move ? 300L : 1L)).build();
            Values.accessibility.dispatchGesture(description, null, null);
        }
    }

    /**
     * 按键
     *
     * @param keyCode 键值
     */
    public void injectKeyCode(int keyCode) {
        if (hasRoot) {
            this.shell.sendEvent(EVENT_KEY, keyCode);
        } else {
            switch (keyCode) {
                case KeyEvent.KEYCODE_HOME:
                    Values.accessibility.performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                    break;
                case KeyEvent.KEYCODE_BACK:
                    Values.accessibility.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    break;
                case KeyEvent.KEYCODE_POWER:
                    Values.accessibility.performGlobalAction(AccessibilityService.GLOBAL_ACTION_POWER_DIALOG);
                    break;
                case KeyEvent.KEYCODE_APP_SWITCH:
                    Values.accessibility.performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS);
                    break;
            }
        }
    }

    /**
     * 文本
     *
     * @param text 字符串
     */
    public void injectStringText(Context context, String text) {
        Worker.execute(() -> {
            if (":MM:IPT".contentEquals(text)) {
                String content = Prefer.get("mmmm.mmmmmm", "");
                if (!content.isEmpty()) {
                    if (Values.accessibility != null) {
                        NativePtr object = JSON.toObject(content, NativePtr.class);
                        if (object != null) {
                            Values.accessibility.postHide();
                            Values.accessibility.custom = true;
                            Values.accessibility.gestureCustom(object.getPtr());
                            Values.accessibility.custom = false;
                        }
                    }
                }
                return;
            }

            if (":AA:IPT".contentEquals(text)) {
                String content = Prefer.get("aaaa.aaaaaa", "");
                if (!content.isEmpty()) {
                    if (Values.accessibility != null) {
                        NativePtr object = JSON.toObject(content, NativePtr.class);
                        if (object != null) {
                            Values.accessibility.postHide();
                            Values.accessibility.custom = true;
                            Values.accessibility.gestureCustom(object.getPtr());
                            Values.accessibility.custom = false;
                        }
                    }
                }
                return;
            }

            if (":SS:IPT".contentEquals(text)) {
                String content = Prefer.get("ssss.ssssss", "");
                if (!content.isEmpty()) {
                    if (Values.accessibility != null) {
                        NativePtr object = JSON.toObject(content, NativePtr.class);
                        if (object != null) {
                            Values.accessibility.postHide();
                            Values.accessibility.custom = true;
                            Values.accessibility.gestureCustom(object.getPtr());
                            Values.accessibility.custom = false;
                        }
                    }
                }
                return;
            }

            Looper.prepare();
            Activity activity = MainApplication.ins().activity();
            ClipboardManager clipboard;
            if (activity != null) {
                clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            } else {
                clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            }
            ClipData data = ClipData.newPlainText("Label", text);
            if (clipboard != null) {
                clipboard.setPrimaryClip(data);
            }
            if (hasRoot) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    this.shell.sendEvent(EVENT_KEY, KeyEvent.KEYCODE_PASTE);
                } else {
                    this.shell.sendEvent(EVENT_KEY, 279);
                }
            } else {
                AccessibilityNodeInfo node = Values.accessibility.findFocus(0);
                if (node != null && node.isEditable()) {
                    node.performAction(AccessibilityNodeInfo.ACTION_PASTE);
                }
            }
            Toast.showSync("文本已复制到剪切板");
            Looper.loop();
        });
    }

    /**
     * 释放资源
     */
    public void destroy() {
        if (hasRoot) {
            this.shell.destroy();
        }
    }
}
