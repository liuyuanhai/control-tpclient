package com.dd.rclient.touch;

import android.net.LocalServerSocket;
import android.net.LocalSocket;

import com.dd.rclient.MainApplication;
import com.dd.rclient.logger.Log;
import com.dd.rclient.setting.Settings;
import com.dd.rclient.threads.Worker;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Created by: Administrator.
 * Created date: 2019/10/7.
 * Description: 命令执行
 */
public class Shell {

    // 关闭事件 结束
    private final static String EVENT_CLOSE = "CLOSE";
    private Process exec;
    private DataOutputStream dos;
    private StringBuilder strings;
    private LocalServerSocket socket;
    private LocalSocket accept;
    private OutputStream os;
    private OutputStreamWriter osw;
    private BufferedWriter bw;
    private PrintWriter writer;
    private boolean isConnected = false;

    /**
     * 初始化
     */
    public Shell() {
        try {
            this.strings = new StringBuilder();
            this.exec = Runtime.getRuntime().exec("su");
            this.dos = new DataOutputStream(exec.getOutputStream());
            this.socket = new LocalServerSocket(Settings.SOCKET_ADDRESS);
            this.startLocaleServer();
            this.setClasspath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置环境
     */
    private void setClasspath() {
        this.execute("export", String.format("CLASSPATH=%s", MainApplication.ins().getPackageResourcePath()));
        this.execute("app_process", "/system/bin", TouchProcess.class.getCanonicalName());
    }

    /**
     * 发送事件
     *
     * @param event 事件类型
     * @param data  参数
     */
    public void sendEvent(String event, Object data) {
        this.send(event, data);
    }

    /**
     * 发送事件
     *
     * @param event 事件类型
     * @param x     参数
     * @param y     参数
     */
    public void sendTouchEvent(String event, Object x, Object y) {
        this.send(event, x, y);
    }

    /**
     * 连接到本地通信
     */
    private void startLocaleServer() {
        Worker.execute(() -> {
            while (socket != null && !isConnected) {
                try {
                    this.accept = this.socket.accept();
                    this.accept.setReceiveBufferSize(500000);
                    this.accept.setSendBufferSize(500000);
                    this.os = this.accept.getOutputStream();
                    this.osw = new OutputStreamWriter(this.os);
                    this.bw = new BufferedWriter(this.osw);
                    this.writer = new PrintWriter(this.bw);
                    this.isConnected = true;
                    Log.d("Locale Socket connected");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 关闭连接
     */
    private void closeSocket() throws IOException {
        if (this.writer != null) {
            this.writer.println(Shell.EVENT_CLOSE);
            this.writer.flush();
        }
        if (this.os != null) {
            this.os.close();
        }
        if (this.osw != null) {
            this.osw.close();
        }
        if (this.bw != null) {
            this.bw.close();
        }
        if (this.writer != null) {
            this.writer.close();
        }
        if (this.accept != null) {
            this.accept.close();
        }
        if (this.socket != null) {
            this.socket.close();
            this.socket = null;
        }
    }

    /**
     * 发送数据
     *
     * @param cmd 数据命令
     */
    private void send(Object... cmd) {
        this.strings.setLength(0);
        for (Object object : cmd) {
            if (this.strings.length() != 0) {
                this.strings.append(":");
            }
            this.strings.append(object);
        }
        this.strings.append("\n");
        if (this.writer != null) {
            this.writer.println(this.strings.toString());
            this.writer.flush();
        }
    }

    /**
     * 执行
     *
     * @param cmd 命令
     */
    private void execute(Object... cmd) {
        this.strings.setLength(0);
        for (Object object : cmd) {
            if (this.strings.length() != 0) {
                this.strings.append(" ");
            }
            this.strings.append(object);
        }
        this.strings.append("\n");
        try {
            if (this.dos != null) {
                this.dos.writeBytes(this.strings.toString());
                this.dos.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 资源释放
     */
    public void destroy() {
        try {
            this.closeSocket();
            if (this.dos != null) {
                this.dos.close();
            }
            if (this.exec != null) {
                this.exec.destroy();
            }
            this.isConnected = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
